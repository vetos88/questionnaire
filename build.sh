#!/usr/bin/env bash
set -ex

# Директории проектов
: ${AIOHTTP_DIR:="$(pwd)/aiohttp_api"}
: ${M_CACHE_DIR:="$(pwd)/mcache"}
: ${WEB_APP_DIR:="$(pwd)/web-app"}
: ${LOOP_PUSH_DIR:="$(pwd)/loop_push"}

# Имя проекта
: ${PROJECT_NAME:="questionnaire"}

# Именование образов
: ${AIOHTTP_API_IMAGE:="aiohttp_api"}
: ${M_CACHE_IMAGE:="m_cache"}
: ${WEB_APP_IMAGE:="web-app"}
: ${LOOP_PUSH_IMAGE:="loop_push"}

# Отслеживание версионности исходников
: ${GIT_COMMIT:=$(git rev-parse HEAD)}
: ${GIT_BRANCH:=$(git branch | grep \* | cut -d ' ' -f2)}
: ${TAG_VERSION:=$(git describe --tags --abbrev=0 ${GIT_COMMIT})}

echo "Building images of ${TAG_VERSION} version and ${GIT_COMMIT} commit ..."

# Сборка образов
IMAGES=(
    ${AIOHTTP_API_IMAGE} ${AIOHTTP_DIR} \
    ${M_CACHE_IMAGE} ${M_CACHE_DIR} \
    ${WEB_APP_IMAGE} ${WEB_APP_DIR} \
    ${LOOP_PUSH_IMAGE} ${LOOP_PUSH_DIR}
)

for ((i=0; i<${#IMAGES[@]}; i+=2)); do
(
    echo "Building ${IMAGES[i]}:${TAG_VERSION}";
    docker build --file ${IMAGES[i + 1]}/Dockerfile \
             --force-rm \
             --tag "${PROJECT_NAME}/${IMAGES[i]}:${TAG_VERSION}" \
             --build-arg GIT_COMMIT=${GIT_COMMIT} \
             --build-arg GIT_BRANCH=${GIT_BRANCH} \
             --compress \
             --rm ${IMAGES[i + 1]}
) &
done

wait
