#!/usr/bin/env bash
set -ex

# Версии образов
: ${GIT_COMMIT:=$(git rev-parse HEAD)}
: ${GIT_BRANCH:=$(git branch | grep \* | cut -d ' ' -f2)}
: ${AIOHTTP_API_V:=$(git describe --tags --abbrev=0 ${GIT_COMMIT})}
: ${MCACHE_V:=$(git describe --tags --abbrev=0 ${GIT_COMMIT})}
: ${WEB_APP_V:=$(git describe --tags --abbrev=0 ${GIT_COMMIT})}
: ${LOOP_PUSH_V:=$(git describe --tags --abbrev=0 ${GIT_COMMIT})}

# Имя проекта
: ${PROJECT_NAME:="questionnaire"}

# Именование образов
: ${AIOHTTP_API_IMAGE:="aiohttp_api"}
: ${M_CACHE_IMAGE:="m_cache"}
: ${WEB_APP_IMAGE:="web-app"}
: ${LOOP_PUSH_IMAGE:="loop_push"}

# Настройки инфраструктуры
: ${REGISTRY_HOST:="registry.zoloto585.int"}
: ${DOMAIN_NAME:="questionnaire.service.consul.zoloto585.int"}
: ${REGISTRY_PORT:="10443"}
: ${REMOTE_HOSTNAME:="spb-tl-roz3calc01.zoloto585.int"}
: ${REMOTE_USER:="prognoz"}


## Сборка образов
#echo "Run building script ..."
#bash $(pwd)/build.sh

if [[ ${1} == "stack" ]] ; then

    # Загрузка собранных образов на удаленный registry
    echo "Sending images to remote registry ..."

    for IMAGE_ID in $(docker images --filter "label=git_commit=${GIT_COMMIT}" --format "{{.Repository}}:{{.Tag}}" | grep -v "${REGISTRY_HOST}:${REGISTRY_PORT}"); do
    (
        REMOTE_IMAGE_ID="${REGISTRY_HOST}:${REGISTRY_PORT}/${IMAGE_ID}"

        docker tag ${IMAGE_ID} ${REMOTE_IMAGE_ID}
        docker push ${REMOTE_IMAGE_ID}
    ) &
    done

    wait

    {
        # Удалим сокет перед использованием
        rm -f /tmp/docker.sock

        # Подключение к удаленному Docker Engine
        (
            ssh -nNT -L /tmp/docker.sock:/var/run/docker.sock ${REMOTE_USER}@${REMOTE_HOSTNAME}
        ) &
        sleep 10  # Ждём пока подключается SSH

        DOCKER_HOST=unix:///tmp/docker.sock
        export DOCKER_HOST

        # Экспорт переменных окружения
        export REGISTRY_HOST
        export REGISTRY_PORT
        export PROJECT_NAME
        export M_CACHE_IMAGE
        export MCACHE_V
        export AIOHTTP_API_IMAGE
        export AIOHTTP_API_V
        export DOMAIN_NAME
        export WEB_APP_IMAGE
        export WEB_APP_V
        export LOOP_PUSH_IMAGE
        export LOOP_PUSH_V

        {
            docker network create web-apps-network --driver overlay --attachable
        } || {
            echo "Network already exist ..."
        }

        # Разворачивание образов
        docker stack deploy -c "docker-compose.${1}.yml" ${PROJECT_NAME} --with-registry-auth

        rm -f /tmp/docker.sock
    } || {
        # catch block
        echo "Can't to run compose file on remote side..."
        rm -f /tmp/docker.sock
    }

elif [[ ${1} == "local" ]] ; then

    # Экспорт переменных окружения
    export PROJECT_NAME
    export M_CACHE_IMAGE
    export MCACHE_V
    export AIOHTTP_API_IMAGE
    export AIOHTTP_API_V
    export DOMAIN_NAME
    export WEB_APP_IMAGE
    export WEB_APP_V
    export LOOP_PUSH_IMAGE
    export LOOP_PUSH_V

    # Разворачивание образов
#    docker-compose --project-name ${PROJECT_NAME} \
#                   --file "docker-compose.stack.yml" \
#                   up -d --force-recreate --remove-orphans "${@:2}"
#    for IMAGE_ID in $(docker images --filter "label=git_commit=${GIT_COMMIT}" --format "{{.Repository}}:{{.Tag}}" | grep -v "${REGISTRY_HOST}:${REGISTRY_PORT}"); do
#    (
#        REMOTE_IMAGE_ID="${REGISTRY_HOST}:${REGISTRY_PORT}/${IMAGE_ID}"
#
#        docker tag ${IMAGE_ID} ${REMOTE_IMAGE_ID}
#    ) &
#    done
    echo "${REGISTRY_HOST}:${REGISTRY_PORT}/${PROJECT_NAME}/${WEB_APP_IMAGE}:${WEB_APP_V}"
    echo "${REGISTRY_HOST}:${REGISTRY_PORT}/${PROJECT_NAME}/${AIOHTTP_API_IMAGE}:${AIOHTTP_API_V}"
    echo "${REGISTRY_HOST}:${REGISTRY_PORT}/${PROJECT_NAME}/${M_CACHE_IMAGE}:${MCACHE_V}"
    docker stack deploy -c "docker-compose.stack.yml" ${PROJECT_NAME} --with-registry-auth

fi
