#!/usr/bin/env bash
# Менеджер управления процесами развертывания и эксплуатации.
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'
# Список доступных команд. Команда передается первым аргументом.
commands=(
"help"
"run"
"status"
"stop"
)
set -ex

# Именование образов
WEB_APP_IMAGE="web-app"
AIOHTTP_API_IMAGE="aiohttp_api"
LOOP_PUSH_IMAGE="loop_push"
REDIS_IMAGE="redis:5.0.3"

# Имя проекта
PROJECT_NAME="questionnaire"

# Настройки инфраструктуры
registry_host="registry.zoloto585.int:10443"


if [[ " ${commands[@]} " =~ " $1 " ]]; then
    if [[ "$1" == ${commands[0]} ]]; then
    # Список всех команд.
        echo -e "${GREEN}Аvaliable commands:${NC}"
        echo ${commands[*]}
    else
        echo -e "${GREEN} $1 - command was provided.${NC}"
        if [[ "$1" == ${commands[1]} ]]; then
            if [[ -f stack_config  ]]; then
                source ./stack_config
            else
                echo -e "${YELLOW}stack_config file not found!${NC} ${RED}Deploy not possible!${NC}"
                exit 126
            fi
            if [[ -f aiohttp_api_main.ini  ]] && [[ -f loop_poosh_main.ini ]]; then
                echo -e "${GREEN} Config files WAS found,${NC} custom settings provided."
            else
                echo -e "${YELLOW}Config files not found!${NC} ${RED}Deploy not possible!${NC}"
            fi
            registry=${CUSTOM_REGISTRY:-$registry_host}
            echo -e "${YELLOW} network web-apps-network (overlay) will create${NC}"
            # Создание сети для стека приложения
            {
            docker network create web-apps-network --driver overlay --attachable
            } || {
                echo "Network already exist ..."
            }
            # Пересоздание конфига для приложения
            if [[ -n "$registry" ]]; then
                # Подключение к приватному репозиторию
                echo -e "Registry address provided: ${YELLOW} ${registry} ${NC} user: ${YELLOW} ${REGISTRY_USER} ${NC} deploy from registry"
                docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASS} https://${registry}/
            fi
            # Запуск стека приложения
            export WEB_APP_IMAGE=${registry}/${PROJECT_NAME}/${WEB_APP_IMAGE}:${WEB_APP_VERSION}
            export AIOHTTP_API_IMAGE=${registry}/${PROJECT_NAME}/${AIOHTTP_API_IMAGE}:${AIOHTTP_API_VERSION}
            export LOOP_PUSH_IMAGE=${registry}/${PROJECT_NAME}/${LOOP_PUSH_IMAGE}:${LOOP_POOSH_VERSION}
            echo -e "WEB-APP from image with tag ${GREEN}${WEB_APP_IMAGE} ${NC} will start!"
            echo -e "AIOHTTP-API from image with tag ${GREEN}${AIOHTTP_API_IMAGE} ${NC} will start!"
            echo -e "LOOP-PUSH from image with tag ${GREEN}${LOOP_PUSH_IMAGE} ${NC} will start!"
            echo -e "STORAGE from image with tag ${GREEN}${REDIS_IMAGE} ${NC} will start!"
            export SERVICE_NAME
            export DEPLOY_ENVIRONMENT
            export APP_DATA_FOLDER
            export REDIS_IMAGE
            docker stack deploy -c docker-compose.yml --with-registry-auth ${PROJECT_NAME}
            echo "Start app."
            docker stack ps ${PROJECT_NAME}
        elif [[ $1 == ${commands[2]} ]]; then
            echo -e "${YELLOW} ${PROJECT_NAME} stack status ${NC}"
            docker stack ps ${PROJECT_NAME}
            echo -e "${YELLOW} ${PROJECT_NAME} services status ${NC}"
            docker service ls | grep ${PROJECT_NAME}
        elif [[ $1 == ${commands[3]} ]]; then
            # Остановка стека приложения
            echo -e "${YELLOW} ${PROJECT_NAME} stack app will shut down ${NC}"
            docker stack rm ${PROJECT_NAME}
            docker stack ps ${PROJECT_NAME}
        fi
    fi
else
    echo -e "${RED}$0 $1 - Bad command provided!${NC} ${YELLOW}Provide one of: [${commands[*]}]. ${NC}"
fi
