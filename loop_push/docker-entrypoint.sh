#!/usr/bin/env bash
set -e

if [[ "${1}" == "subscriber" ]]; then
    python3 app.py
fi