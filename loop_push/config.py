# -*- coding: utf-8 -*-
from functools import partial
from raven_aiohttp import QueuedAioHttpTransport

import os
import configparser


def _try_convert(value: object, default: object, *types: tuple) -> object:
    """
    Конвертирует значение, если не выходит
    вернуть значение, возвращается
    значение поумолчанию.
    :param value: Конвертируемое значение
    :param default: Значение по-умолчанию
    :param types: Типы данных
    :return: Значение сконвертированное
    """
    if value is None:
        if callable(default):
            return default()

        return default

    for t in types:
        try:
            return t(value)
        except (TypeError, ValueError) as ex:
            continue

    return default


def _raise_config(field_name: str) -> None:
    """
    Генерирует исключение для
    поля в конфигурационном листе,
    если поле отмеченно как обязательное.
    :param field_name: Имя поля
    :return: None
    """
    raise Exception("Required option of field {}".format(field_name))


def create_config():
    """
    Создает класс конфигурации бэкэнд проекта.
    В приоритете переменные из os модуля и пользовательского
    окружения.
    :return:
    """
    CONFIG = configparser.ConfigParser(interpolation=None)
    CONFIG.read('main.ini')

    class Singleton(type):
        _instances = {}

        def __call__(cls, *args, **kwargs):
            if cls not in cls._instances:
                cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            return cls._instances[cls]

    class Config(metaclass=Singleton):

        # Опции Sentry
        SENTRY_TRANSPORT = ('SENTRY', 'Transport', 'http', str)
        SENTRY_HOST = ('SENTRY', 'Host', None, str)
        SENTRY_PORT = ('SENTRY', 'Port', 9300, int)
        SENTRY_PUBLIC_KEY = ('SENTRY', 'PublicKey', None, str)
        SENTRY_SECRET_KEY = ('SENTRY', 'SecretKey', None, str)
        SENTRY_PROJECT_VHOST = ('SENTRY', 'ProjectVHost', None, str)

        # Опции внешней БД
        PROFILE_URL = ('STORE', 'ProfileURL', lambda: _raise_config('ProfileURL'), str)

        # Опции логирования
        LOGGING_LEVEL = ('LOGGING', 'Level', 'INFO', str)
        LOGGING_FORMAT = ('LOGGING', 'Format', '[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s', str)
        LOGGING_DATEFMT = ('LOGGING', 'DateFMT', '%Y-%m-%d %H:%M:%S %z', str)

        # Опции БД
        REDIS_HOST = ('REDIS', 'Host', lambda: _raise_config('REDIS_HOST'), str)
        REDIS_PORT = ('REDIS', 'Port', 6379, int)

        # Опции DaData API
        DADATA_API_KEY = ('DADATA', 'APIKey', lambda: _raise_config('DADATA_API_KEY'), str)
        DADATA_SECRET_KEY = ('DADATA', 'SecretKey', lambda: _raise_config('DADATA_SECRET_KEY'), str)

        def __new__(cls, *args, **kwargs):
            for k, v in cls.__dict__.items():
                if isinstance(v, tuple) and len(v) == 4:
                    setattr(
                        cls, k, _try_convert(
                            os.getenv(k, CONFIG[v[0]].get(v[1])), v[2], v[3]
                        )
                    )

        @classmethod
        def logging(cls):
            return dict(
                level=cls.LOGGING_LEVEL,
                format=cls.LOGGING_FORMAT,
                datefmt=cls.LOGGING_DATEFMT
            )

        @classmethod
        def sentry(cls):
            return dict(
                dsn='{}://{}:{}@{}:{}/{}'.format(
                    cls.SENTRY_TRANSPORT,
                    cls.SENTRY_PUBLIC_KEY,
                    cls.SENTRY_SECRET_KEY,
                    cls.SENTRY_HOST,
                    cls.SENTRY_PORT,
                    cls.SENTRY_PROJECT_VHOST,
                ),
                transport=partial(QueuedAioHttpTransport, workers=4, qsize=1000)
            )

        @classmethod
        def redis(cls):
            return dict(
                address=(cls.REDIS_HOST, cls.REDIS_PORT),
                db=1,
                encoding='utf-8',
                minsize=5,
                maxsize=10
            )

    _ = Config()
    return Config


Config = create_config()