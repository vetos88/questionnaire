#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import ujson
import asyncio

import aiohttp
import aioredis
import logging

from raven import Client as Sentry
from aiohttp import request

import config


UUID_PATTERN = re.compile('(?P<uuid>[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12})')


async def send_to_storage(data: dict) -> None:
    """
    Отправка данных анкеты в 1С систему.
    :param data: структура анкеты
    :return: as is
    """
    if data.get('processingPersonalDataAgreement') is True:
        async with request(
            url=config.Config.PROFILE_URL,
            method='POST',
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            data=ujson.dumps(data)
        ) as response:

            if response.status > 299:
                detail = await response.text()
                logging.info(f'reason: {response.reason}, detail: ${detail}')
            response.raise_for_status()
            logging.debug(
                (await response.json(loads=ujson.loads))
            )


async def check_queue(pool: aioredis.ConnectionsPool, sentry_client: Sentry) -> None:
    """
    Извлекаем данные из очереди сообщений,
    если таковые имеются в канале очереди
    сообщений. Если нет, то подписываемся
    на канал users.
    :param pool: пул соединений
    :param sentry_client: клиент Sentry
    :return: as is
    """
    try:

        with await pool as conn:
            while True:
                # Выбираем случайный ID (
                #   алгоритм выборки Knuth's algorithm S - https://rosettacode.org/wiki/Knuth%27s_algorithm_S
                # )
                user_id = await conn.spop('users.s_queue')
                if user_id:
                    logging.info('Incoming message from queue users: {}'.format(user_id))
                    try:
                        val = await conn.get(user_id)
                        await send_to_storage(
                            {
                                **{'uuid': UUID_PATTERN.search(user_id).group('uuid')},
                                **(ujson.loads(val))
                            }
                        )
                    except (TypeError, AttributeError, IndexError) as e:
                        # При данной ошибке не стоит
                        # сохранять UUID анкеты, т.к. может
                        # являтся не сериализованной
                        sentry_client.captureException()
                        logging.exception(e)

                        # Или схема идентификации пользователя
                        # не содержит схемы UUID

                    except Exception as e:
                        # Логируем и отправляем UUID обратно в очередь
                        # до следующей попытки
                        sentry_client.captureException()
                        logging.exception(e.request_info)

                        # В "хвост" очереди
                        _ = await conn.sadd('users.s_queue', user_id)

                        # Задрежка при следующей попытке.
                        # Ждём пока 1С поднимется
                        await asyncio.sleep(5)
                else:
                    await asyncio.sleep(5)

    except Exception as e:
        sentry_client.captureException()
        logging.exception(e)

    finally:
        # Дружественно закроем все подключения
        pool.close()

        # Ждем пока сообщение об ошибке отправится на сервер Sentry
        await asyncio.sleep(5)


async def move_from_list_to_set(pool: aioredis.ConnectionsPool, sentry_client: Sentry) -> None:
    """
    Ранее в очереди использовался
    список, который дублировал записи.

    Данная функция перезагружает данные
    из списка в неупорядоченную хэш-таблицу.
    :param conn:
    :return:
    """
    try:
        with await pool as conn:

            user_id = await conn.rpop('users.queue')
            while user_id:
                logging.debug('Moving message from users.queue to users.s_queue: {}'.format(user_id))
                _ = await conn.sadd('users.s_queue', user_id)
                user_id = await conn.rpop('users.queue')

    except Exception as e:
        sentry_client.captureException()
        logging.exception(e)

        # Ждем пока сообщение об ошибке отправится на сервер Sentry
        await asyncio.sleep(5)


async def create_app() -> aioredis.ConnectionsPool:
    """
    Функция фабричная приложения.
    :return: connection pool of Redis instance
    """

    # Enabling local logging
    logging.basicConfig(
        **config.Config.logging()
    )

    # Initialize redis instance
    pool = await aioredis.create_redis_pool(
        **config.Config.redis()
    )

    return dict(
        pool=pool,
        sentry_client=Sentry(**config.Config.sentry())
    )


def main():
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(
        create_app()
    )

    loop.run_until_complete(
        move_from_list_to_set(**app)
    )

    loop.run_until_complete(
        check_queue(**app)
    )


if __name__ == '__main__':
    main()
