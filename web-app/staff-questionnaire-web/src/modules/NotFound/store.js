/**
* Модуль стора компонента
*/

export default {
  state: {
    l18n: Object.freeze({
      goHome: 'На главную',
      pageNotFound: 'Страница не найдена'
    })
  },
  mutations: {},
  actions: {},
  getters: {
    goHomeText (state) {
      return state.l18n.goHome
    },
    pageNotFoundText (state) {
      return state.l18n.pageNotFound
    }
  }
}
