/**
 * Модуль стора компонента
 */
import Api from '@/services/api'
import {EMAIL_FORMAT} from '@/modules/CandidateQuestionnaire/validatorsAndFormatters'

/**
 * Обьекст с переменными интернационализации
*/

/**
 * Шаблон дя подстановки параметров
*/
const reForParams = /{}/

const l18n = {
  startedPageEmailLabelText: 'Введите адрес электронной почты для получения ссылки на анкету',
  startedPageEmailPlaceholderText: EMAIL_FORMAT,
  startedPageEmailHintText: 'Адрес электронной почты',
  getMailLinkQuestionnaireButtonLabelText: 'Нажмите для получения ссылки на анкету',
  sendMailFailedFailedText: 'Не удалось отправить письмо. Попробуйте еще раз.',
  startedPageEmailRetryCountdownTimer: 'Повторная отправка возможна через {}',
  checkValidDataErrorText: 'Проверте корректоность введенных данных',
  emailQuestionnaireLinkWasSentText: 'Письмо со ссылкой будет отпралено Вам на электронную почту. ' +
    'Для заполнения анкеты перейдите по ссылке из письма',
  emailQuestionnaireLinkSendingErrorText: 'Произошла ошибка при обработке запроса. ' +
    'Попробуйте подождать некоторое время и отправить запрос еще раз, либо укажите другой email.'
}

/**
 * Обьект для генерации гетеров
*/
const autoGetters = {
  startedPageEmailRetryCountdownTimerText (state) {
    return countdown => state.l18n.startedPageEmailRetryCountdownTimer.replace(reForParams, countdown)
  }
}

/**
 * Дополнительные ключи в основной стейт и формирование геттеров
*/
Object.keys(l18n).forEach(langValueKey => {
  autoGetters[langValueKey] = (state) => state.l18n[langValueKey]
})

/**
 * Стор компонента
*/
export default {
  state: {
    l18n: Object.freeze(l18n)
  },
  mutations: {
  },
  actions: {
    async sendMailWithNewQuestionnaireLink ({commit, getters, dispatch}, email) {
      commit('setLoading', true)
      try {
        const data = await Api.getNewQuestionnaireLinkByEmail(email)
        console.debug('getNewQuestionnaireLinkByEmail response from server APi data', data)
        // если ошибки не было считаем что эмаил отправлен
        dispatch(
          'setError',
          {
            message: getters.emailQuestionnaireLinkWasSentText,
            level: getters.errorLevels.SUCCESS
          }
        )
      } catch (error) {
        console.warn('sendMailWithNewQuestionnaireLink', error)
        dispatch('setError', getters.emailQuestionnaireLinkSendingErrorText)
        throw new Error('Error was detected')
      } finally {
        dispatch('setLoading', false)
      }
    }
  },
  getters: {
    ...autoGetters
  }
}
