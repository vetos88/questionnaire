/**
 * Модуль стора компонента
 */

/**
 * Обьекст с переменными интернационализации
*/
const l18n = {
  startNewQuestionnaireButtonText: 'Начать заново',
  startNewQuestionnaireToolTipText: 'Перейти на страницу запуска новой анкеты',
  leaveCurrentQuestionnaireAgreementTitleText: 'Вы уверены?',
  leaveCurrentQuestionnaireAgreementDetailText: 'Вы покидаете текущую анкету. ' +
    'Не сохраннёные результаты могут быть потеряны. Подтвердите действие.',
  leaveAgreementYesButtonText: 'Да. перейти к созданию новой анткеты.',
  leaveAgreementNoButtonText: 'Нет. Остаться.'
}

/**
 * Обьект для генерации гетеров
*/
const autoGetters = {}

/**
 * Дополнительные ключи в основной стейт и формирование геттеров
*/
Object.keys(l18n).forEach(langValueKey => {
  autoGetters[langValueKey] = (state) => state.l18n[langValueKey]
})

/**
 * Стор компонента
*/
export default {
  state: {
    l18n: Object.freeze(l18n),
    uuid: null
  },
  mutations: {
    setUuidData (state, uuid) {
      state.uuid = uuid
    }
  },
  actions: {
    setQuestionnaireUuid ({commit}, data) {
      if (!data || typeof data !== 'object') {
        console.warn('setQuestionnaireUuid, data', data)
        throw new Error('Not valid server data! Data is not Object!')
      }
      console.debug('setQuestionnaireUuid data', data)
      let uuid = data.uuid
      if (!uuid) {
        console.warn('setQuestionnaireUuid, data', data)
        throw new Error('Uuid not found')
      }
      console.debug('setQuestionnaireUuid uuid', uuid)
      commit('setUuidData', uuid)
    },
    clearUuid ({commit}) {
      commit('setUuidData', null)
    }
  },
  getters: {
    ...autoGetters,
    getQuestionnaireUuid: (state) => state.uuid
  }
}
