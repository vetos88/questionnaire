/**
* Модуль стора компонента
*/

import Api from '@/services/api'
import {
  state as l18iState,
  getters as l18iGetterg
} from './l18n'
import {
  dateValueIsValid,
  DOCUMENT_RF_PASSPORT,
  DOCUMENT_RESIDENCE_PERMIT,
  DOCUMENT_ANOTHER_COUNTRY_PASSPORT,
  DYNAMIC_SELECTION_EDUCATIONS,
  DYNAMIC_SELECTION_CITIZENSHIP,
  DYNAMIC_SELECTION_FOREIGN_LANGUAGES,
  DYNAMIC_SELECTION_FOREIGN_FOREIGN_LANGUAGES_LEVELS,
  DYNAMIC_SELECTION_INTERESTS,
  DYNAMIC_SELECTION_RELATION_DEGREES
} from './validatorsAndFormatters'
import {
  PreviousDataBlank, RelativeData, PreviousLiveLocation, EducationalInstitution, WorkingOrganization, ForeignLanguage
} from './models'
export {
  PreviousDataBlank, RelativeData, PreviousLiveLocation, EducationalInstitution, WorkingOrganization, ForeignLanguage
}

/**
 * Маппинг классов к коллекциям
*/
const collectionItems = {
  'previousPersonalData': PreviousDataBlank,
  'relativesData': RelativeData,
  'previousLiveLocations': PreviousLiveLocation,
  'educationalInstitutions': EducationalInstitution,
  'workingOrganizations': WorkingOrganization,
  'foreignLanguages': ForeignLanguage
}

const booleanFields = [
  'wasPreviousPersonalData',
  'sameAsRegisteredLive',
  'isDisabled',
  'previousLiveLocationsWas',
  'checkPersonalDataAgreementByPolygraph',
  'badHabitsHaving',
  'criminalConvictionAdministrativeOffenses'
]

const arrayFields = [
  'personalHobby'
]

const selectionItems = {
  currentPersonalDocumentType: DOCUMENT_RF_PASSPORT
}

export default {
  state: {
    ...l18iState,
    dynamicSelections: {
      [DYNAMIC_SELECTION_EDUCATIONS]: [],
      [DYNAMIC_SELECTION_CITIZENSHIP]: [],
      [DYNAMIC_SELECTION_FOREIGN_LANGUAGES]: [],
      [DYNAMIC_SELECTION_FOREIGN_FOREIGN_LANGUAGES_LEVELS]: [],
      [DYNAMIC_SELECTION_INTERESTS]: [],
      [DYNAMIC_SELECTION_RELATION_DEGREES]: []
    },
    candidateSelectedVacancy: null,
    name: '',
    surname: '',
    middleName: '',
    wasPreviousPersonalData: false,
    previousPersonalData: [
      new PreviousDataBlank({})
    ],
    genderSign: null,
    bornLocation: '',
    birthDay: '',
    registeredLiveLocation: '',
    sameAsRegisteredLive: false,
    liveLocation: '',
    maritalStatus: null,
    isDisabled: false,
    disabledGroup: '',
    mobileTelephone: '',
    homeTelephone: '',
    workTelephone: '',
    email: '',
    snils: '',
    inn: '',
    // История проживания
    previousLiveLocationsWas: true,
    previousLiveLocations: [
      new PreviousLiveLocation({})
    ],
    // Документы
    currentPersonalDocumentType: DOCUMENT_RF_PASSPORT,
    passportSeries: null,
    passportNumber: null,
    passportIssueDate: null,
    passportIssueOrganization: '',
    residencePermitSeries: null,
    residencePermitNumber: null,
    residencePermitIssueDate: null,
    residencePermitIssueOrganization: '',
    anotherCountryCitizenship: '',
    anotherCountryPassportSeries: null,
    anotherCountryPassportNumber: null,
    anotherCountryPassportIssueDate: null,
    anotherCountryPassportIssueOrganization: '',
    // Родственники
    relativesData: [
      new RelativeData({})
    ],
    // Образование
    educationalInstitutions: [
      new EducationalInstitution({})
    ],
    // Опыт работы
    workingOrganizations: [
      new WorkingOrganization({})
    ],
    // Дополнительные вопросы
    pcKnowledgeLevel: '',
    whereFindVacancy: '',
    additionalNotation: '',
    personalHobby: [],
    salaryLevelWantedNow: null,
    salaryLevelWantedBeforeYear: null,
    salaryLevelWantedBeforeThreeYears: null,
    checkPersonalDataAgreementByPolygraph: false,
    badHabitsHaving: false,
    criminalConvictionAdministrativeOffenses: false,
    // Владение языками
    foreignLanguages: [
      new ForeignLanguage({})
    ],
    processingPersonalDataAgreement: false
  },
  mutations: {
    setPreviousDataState (state, payload) {
      state.wasPreviousPersonalData = payload
    },
    addNewPreviousDataBlank (state, newBlank) {
      state.previousPersonalData.push(newBlank)
    },
    removePreviousDataBlank (state, arrayWithoutRemovedElement) {
      state.previousPersonalData.splice(arrayWithoutRemovedElement, 1)
    },
    addRelativeData (state, newBlank) {
      state.relativesData.push(newBlank)
    },
    removeRelativeData (state, arrayWithoutRemovedElement) {
      state.relativesData.splice(arrayWithoutRemovedElement, 1)
    },
    addItemList (state, {collection, newItem}) {
      console.debug('addItemList collection', collection)
      console.debug('addItemList newItem', newItem)
      collection.push(newItem)
    },
    removeItemList (state, {collection, index}) {
      collection.splice(index, 1)
    },
    setSurname (state, surname) {
      state.surname = surname
    },
    setName (state, name) {
      state.name = name
    },
    setMiddleName (state, middleName) {
      state.middleName = middleName
    },
    setPreviousDataFieldValue (state, {currentDataBlank, fieldName, value}) {
      currentDataBlank[fieldName] = value
    },
    setRelativeValue (state, {currentRelative, fieldName, value}) {
      currentRelative[fieldName] = value
    },
    setCollectionItemValue (state, {currentItem, fieldName, value}) {
      currentItem[fieldName] = value
    },
    setUserInputValue (state, {fieldName, value}) {
      state[fieldName] = value
    },
    setActualPersonalDocument (state, newActualDocument) {
      state.currentPersonalDocumentType = newActualDocument
    },
    setUserAgreement (state, isUserAgree) {
      state.processingPersonalDataAgreement = isUserAgree
    },
    setDynamicSelectionCollectionData (state, {collection, array}) {
      state.dynamicSelections[collection] = []
      array
        .filter(el => el.uuid && el.text)
        .forEach(el => state.dynamicSelections[collection].push(Object.freeze(el)))
    }
  },
  actions: {
    async saveQuestionnaireData ({ commit, getters, dispatch }, userUuid) {
      commit('setLoading', true)
      try {
        const data = await Api.saveQuestionnaireData(userUuid, getters.allQuestionnaireData)
        console.debug('saveQuestionnaireData response from server APi data', data)
        dispatch('setError', {level: getters.errorLevels.SUCCESS, message: getters.dataSuccessSavedMessageText})
      } catch (error) {
        console.error('saveQuestionnaireDataError', error)
        dispatch('setError', {level: getters.errorLevels.WARN, message: getters.saveValidationDataErrorText})
      } finally {
        commit('setLoading', false)
      }
    },
    async getQuestionnaireData ({ commit, getters, dispatch }, userUuid) {
      commit('setLoading', true)
      try {
        const rawServerData = await Api.getQuestionnaireData(userUuid)
        console.debug('getQuestionnaireData response from server APi data', rawServerData)
        dispatch('fillQuestionnaireData', rawServerData)
      } catch (error) {
        console.warn('getQuestionnaireData broken userUuid', userUuid)
        console.error('getQuestionnaireDataError', error)
        dispatch('setError', getters.questionnaireFetchingErrorText(userUuid))
        throw new Error('Broken uuid. Cant fetch questionnaire')
      } finally {
        commit('setLoading', false)
      }
    },
    fillQuestionnaireData ({commit, getters, dispatch}, rawServerData) {
      console.debug('fillQuestionnaireData rawServerData', rawServerData)
      if (!rawServerData ||
        typeof rawServerData !== 'object' ||
        !rawServerData.scheme ||
        typeof rawServerData.scheme !== 'object'
      ) {
        console.warn('fillQuestionnaireData broken rawServerData', rawServerData)
        dispatch(
          'setError',
          { message: getters.questionnaireDataParsingErrorText,
            level: getters.errorLevels.WARN
          })
        return
      }
      const expectedFields = getters.allQuestionnaireData
      for (let field in expectedFields) {
        if (expectedFields.hasOwnProperty(field)) {
          if (rawServerData.scheme[field] !== undefined) {
            if (collectionItems[field] !== undefined) {
              if (Array.isArray(rawServerData.scheme[field])) {
                commit(
                  'setUserInputValue',
                  {
                    fieldName: field,
                    value: rawServerData.scheme[field].map(el => collectionItems[field].createObject(el))
                  }
                )
              }
            } else {
              commit('setUserInputValue', {fieldName: field, value: rawServerData.scheme[field]})
            }
          } else {
            if (collectionItems[field] !== undefined) {
              let CollectionItemClass = collectionItems[field]
              commit('setUserInputValue', {fieldName: field, value: [new CollectionItemClass({})]})
            } else if (booleanFields.indexOf(field) !== -1) {
              commit('setUserInputValue', {fieldName: field, value: false})
            } else if (arrayFields.indexOf(field) !== -1) {
              commit('setUserInputValue', {fieldName: field, value: []})
            } else if (selectionItems[field] !== undefined) {
              commit('setUserInputValue', {fieldName: field, value: selectionItems[field]})
            } else {
              commit('setUserInputValue', {fieldName: field, value: null})
            }
          }
        }
      }
    },
    async getExternalCollectionsSelectedData ({commit, getters, dispatch}, userUuid) {
      commit('setLoading', true)
      try {
        const collectionsData = await Api.getExternalSelectionsCollections(userUuid)
        console.debug('getExternalCollectionsSelectedData response from server collectionsData', collectionsData)
        dispatch('collectionsQuestionnaireData', collectionsData)
      } catch (error) {
        console.warn('getExternalCollectionsSelectedData broken userUuid', userUuid)
        console.error('getExternalCollectionsSelectedData error', error)
        dispatch('setError', getters.questionnaireFetchingErrorText(userUuid))
        throw new Error('Broken uuid. Cant fetch collections')
      } finally {
        commit('setLoading', false)
      }
    },
    collectionsQuestionnaireData ({commit, getters, dispatch}, collectionsData) {
      if (!collectionsData ||
        typeof collectionsData !== 'object' ||
        !collectionsData.scheme ||
        typeof collectionsData.scheme !== 'object'
      ) {
        console.warn('collectionsQuestionnaireData broken collectionsData', collectionsData)
        dispatch(
          'setError',
          { message: getters.questionnaireDataParsingErrorText,
            level: getters.errorLevels.WARN
          })
        throw new Error('Bad server scheme for collections')
      }
      let collections = collectionsData.scheme
      getters.expectedDynamicSelectionsCollections.forEach(collection => {
        if (Array.isArray(collections[collection])) {
          commit('setDynamicSelectionCollectionData', {collection, array: collections[collection]})
        }
      })
    },
    async getAddressesSuggestions ({ commit, getters }, userPhrase) {
      console.debug('getAddressesSuggestions userUuid', getters.getQuestionnaireUuid)
      console.debug('getAddressesSuggestions userPhrase', userPhrase)
      try {
        const rawServerSuggestion = await Api.getAddressesSuggestions(getters.getQuestionnaireUuid, userPhrase)
        console.debug('getAddressesSuggestions response from server APi data', rawServerSuggestion)
        if (rawServerSuggestion.scheme) {
          return rawServerSuggestion.scheme.suggestions || {}
        } else {
          commit('setError', 'Не верный формат данных от сервера')
        }
      } catch (error) {
        console.error('getAddressesSuggestionsError', error)
        commit('setError', 'Ошибка получения данных')
      }
    },
    changePreviousDataState ({commit, state}, newPreviousDataState) {
      if (state.wasPreviousPersonalData !== newPreviousDataState) {
        commit('setPreviousDataState', newPreviousDataState)
      }
    },
    addNewPreviousDataBlank ({commit}) {
      commit('addNewPreviousDataBlank', new PreviousDataBlank({}))
    },
    removePreviousDataBlank ({commit}, dataBlankIndexInArray) {
      commit('removePreviousDataBlank', dataBlankIndexInArray)
    },
    addRelativeData ({commit}) {
      commit('addRelativeData', new RelativeData({}))
    },
    removeRelativeData ({commit}, relativeIndexInArray) {
      commit('removeRelativeData', relativeIndexInArray)
    },
    addNewItemToCollection ({commit, state}, collectionName) {
      let CollectionItemClass = collectionItems[collectionName]
      let collection = state[collectionName]
      console.debug('addNewItemToCollection CollectionItemClass', CollectionItemClass)
      console.debug('addNewItemToCollection collection', collection)
      if (collection && CollectionItemClass) {
        commit('addItemList', {collection, newItem: new CollectionItemClass({})})
      }
    },
    removeItemList ({commit, state}, {collectionName, index}) {
      let collection = state[collectionName]
      if (collection) {
        commit('removeItemList', {collection, index})
      }
    },
    setActualSurname ({commit}, surname) {
      commit('setSurname', surname)
    },
    setActualName ({commit}, name) {
      commit('setName', name)
    },
    setActualMiddleName ({commit}, middleName) {
      commit('setMiddleName', middleName)
    },
    setPreviousDataBlankFieldValue ({commit, state}, {changedBlankIndex, fieldName, value}) {
      console.debug('setPreviousDataBlankSurname changedBlankIndex', changedBlankIndex)
      console.debug('setPreviousDataBlankSurname fieldName', fieldName)
      let currentDataBlank = state.previousPersonalData[changedBlankIndex]
      if (currentDataBlank) {
        commit('setPreviousDataFieldValue', {currentDataBlank, fieldName, value})
      }
    },
    setRelativeBlockValue ({commit, state}, {changedRelativeIndex, fieldName, value}) {
      console.debug('setRelativeBlockValue changedRelativeIndex', changedRelativeIndex)
      console.debug('setRelativeBlockValue fieldName', fieldName)
      console.debug('setRelativeBlockValue value', value)
      let currentRelative = state.relativesData[changedRelativeIndex]
      if (currentRelative) {
        commit('setRelativeValue', {currentRelative, fieldName, value})
      }
    },
    setValueToListItem ({commit, state}, {collection, index, fieldName, value}) {
      console.debug('setRelativeBlockValue collection', collection)
      console.debug('setRelativeBlockValue index', index)
      console.debug('setRelativeBlockValue fieldName', fieldName)
      console.debug('setRelativeBlockValue value', value)
      let currentItem = state[collection][index]
      if (currentItem) {
        commit('setCollectionItemValue', {currentItem, fieldName, value})
      }
    },
    setUserInputValue ({commit}, {fieldName, value}) {
      commit('setUserInputValue', {fieldName, value})
    },
    changeActualPersonalDocument ({commit}, newActualDocument) {
      commit('setActualPersonalDocument', newActualDocument)
    },
    setProcessingPersonalDataAgreement ({commit}, isUserAgree) {
      commit('setUserAgreement', isUserAgree)
    }
  },
  getters: {
    ...l18iGetterg,
    allQuestionnaireData (state) {
      return {
        candidateSelectedVacancy: state.candidateSelectedVacancy,
        name: state.name,
        surname: state.surname,
        middleName: state.middleName,
        previousPersonalData: state.previousPersonalData,
        wasPreviousPersonalData: state.wasPreviousPersonalData,
        genderSign: state.genderSign,
        bornLocation: state.bornLocation,
        birthDay: state.birthDay,
        registeredLiveLocation: state.registeredLiveLocation,
        sameAsRegisteredLive: state.sameAsRegisteredLive,
        liveLocation: state.liveLocation,
        maritalStatus: state.maritalStatus,
        isDisabled: state.isDisabled,
        disabledGroup: state.disabledGroup,
        mobileTelephone: state.mobileTelephone,
        homeTelephone: state.homeTelephone,
        workTelephone: state.workTelephone,
        email: state.email,
        snils: state.snils,
        inn: state.inn,
        passportSeries: state.passportSeries,
        passportNumber: state.passportNumber,
        passportIssueDate: state.passportIssueDate,
        passportIssueOrganization: state.passportIssueOrganization,
        previousLiveLocationsWas: state.previousLiveLocationsWas,
        previousLiveLocations: state.previousLiveLocations,
        currentPersonalDocumentType: state.currentPersonalDocumentType,
        residencePermitSeries: state.residencePermitSeries,
        residencePermitNumber: state.residencePermitNumber,
        residencePermitIssueDate: state.residencePermitIssueDate,
        residencePermitIssueOrganization: state.residencePermitIssueOrganization,
        anotherCountryCitizenship: state.anotherCountryCitizenship,
        anotherCountryPassportSeries: state.anotherCountryPassportSeries,
        anotherCountryPassportNumber: state.anotherCountryPassportNumber,
        anotherCountryPassportIssueDate: state.anotherCountryPassportIssueDate,
        anotherCountryPassportIssueOrganization: state.anotherCountryPassportIssueOrganization,
        relativesData: state.relativesData,
        educationalInstitutions: state.educationalInstitutions,
        workingOrganizations: state.workingOrganizations,
        pcKnowledgeLevel: state.pcKnowledgeLevel,
        whereFindVacancy: state.whereFindVacancy,
        additionalNotation: state.additionalNotation,
        personalHobby: state.personalHobby,
        salaryLevelWantedNow: state.salaryLevelWantedNow,
        salaryLevelWantedBeforeYear: state.salaryLevelWantedBeforeYear,
        salaryLevelWantedBeforeThreeYears: state.salaryLevelWantedBeforeThreeYears,
        checkPersonalDataAgreementByPolygraph: state.checkPersonalDataAgreementByPolygraph,
        badHabitsHaving: state.badHabitsHaving,
        criminalConvictionAdministrativeOffenses: state.criminalConvictionAdministrativeOffenses,
        foreignLanguages: state.foreignLanguages,
        processingPersonalDataAgreement: state.processingPersonalDataAgreement
      }
    },
    // Информация о вакансии
    candidateSelectedVacancy: (state) => state.candidateSelectedVacancy,
    previousPersonalDataWas (state) {
      return state.wasPreviousPersonalData
    },
    previousPersonalData: (state) => state.previousPersonalData,
    getPreviousDataBlankByIndex (state) {
      return blankIndex => state.previousPersonalData[blankIndex]
    },
    actualName: (state) => state.name,
    actualSurname: (state) => state.surname,
    actualMiddleName: (state) => state.middleName,
    genderSign: (state) => state.genderSign,
    bornLocation: (state) => state.bornLocation,
    birthDay: (state) => state.birthDay,
    registeredLiveLocation: (state) => state.registeredLiveLocation,
    sameAsRegisteredLive: (state) => state.sameAsRegisteredLive,
    liveLocation: (state) => state.liveLocation,
    maritalStatus: (state) => state.maritalStatus,
    isDisabled: (state) => state.isDisabled,
    disabledGroup: (state) => state.disabledGroup,
    mobileTelephone: (state) => state.mobileTelephone,
    homeTelephone: (state) => state.homeTelephone,
    workTelephone: (state) => state.workTelephone,
    email: (state) => state.email,
    snils: (state) => state.snils,
    inn: (state) => state.inn,
    previousLiveLocationsWas: (state) => state.previousLiveLocationsWas,
    previousLiveLocations: (state) => state.previousLiveLocations,
    currentPersonalDocumentType (state) {
      return state.currentPersonalDocumentType
    },
    allAllowedDocuments () {
      return [
        DOCUMENT_RF_PASSPORT,
        DOCUMENT_RESIDENCE_PERMIT,
        DOCUMENT_ANOTHER_COUNTRY_PASSPORT
      ]
    },
    // Паспорт
    passportSeries: (state) => state.passportSeries,
    passportNumber: (state) => state.passportNumber,
    passportIssueDate: (state) => state.passportIssueDate,
    passportIssueOrganization: (state) => state.passportIssueOrganization,
    // Вид на жительство
    residencePermitSeries: (state) => state.residencePermitSeries,
    residencePermitNumber: (state) => state.residencePermitNumber,
    residencePermitIssueDate: (state) => state.residencePermitIssueDate,
    residencePermitIssueOrganization: (state) => state.residencePermitIssueOrganization,
    // Паспорт иностранного гражданина
    anotherCountryCitizenship: (state) => state.anotherCountryCitizenship,
    anotherCountryPassportSeries: (state) => state.anotherCountryPassportSeries,
    anotherCountryPassportNumber: (state) => state.anotherCountryPassportNumber,
    anotherCountryPassportIssueDate: (state) => state.anotherCountryPassportIssueDate,
    anotherCountryPassportIssueOrganization: (state) => state.anotherCountryPassportIssueOrganization,
    // Родственники
    relativesData: (state) => state.relativesData,
    // Образование
    educationalInstitutions: (state) => state.educationalInstitutions,
    // Работа
    workingOrganizations: (state) => state.workingOrganizations,
    // Языки
    foreignLanguages: (state) => state.foreignLanguages,
    processingPersonalDataAgreement: (state) => state.processingPersonalDataAgreement,
    // Дополнительные вопросы
    pcKnowledgeLevel: (state) => state.pcKnowledgeLevel,
    whereFindVacancy: (state) => state.whereFindVacancy,
    additionalNotation: (state) => state.additionalNotation,
    personalHobby: (state) => state.personalHobby,
    salaryLevelWantedNow: (state) => state.salaryLevelWantedNow,
    salaryLevelWantedBeforeYear: (state) => state.salaryLevelWantedBeforeYear,
    salaryLevelWantedBeforeThreeYears: (state) => state.salaryLevelWantedBeforeThreeYears,
    checkPersonalDataAgreementByPolygraph: (state) => state.checkPersonalDataAgreementByPolygraph,
    badHabitsHaving: (state) => state.badHabitsHaving,
    criminalConvictionAdministrativeOffenses: (state) => state.criminalConvictionAdministrativeOffenses,
    // Геттеры динамических выборов
    expectedDynamicSelectionsCollections: (state) => Object.keys(state.dynamicSelections),
    dynamicSelectionsData (state) {
      return (dynamicSelection) => state.dynamicSelections[dynamicSelection]
    },
    /**
     * Просто функция проверки даты для возврата null
    */
    checkAndConvertDate () {
      return rawDate => (dateValueIsValid(rawDate)) ? rawDate : null
    },
    clearValue () {
      return (unknownTypeValue) => {
        let value = unknownTypeValue
        console.debug('clearValue unknownTypeValue', unknownTypeValue)
        if (typeof unknownTypeValue === 'string') {
          value = unknownTypeValue.trim()
        }
        if (Array.isArray(unknownTypeValue)) {
          value = unknownTypeValue
        } else if (value && typeof unknownTypeValue === 'object') {
          // если обьект предполагаем что это событие
          value = unknownTypeValue.target.value.trim()
        }
        if (typeof unknownTypeValue === 'boolean') {
          value = unknownTypeValue
        }
        console.debug('clearValue value', value)
        return value
      }
    }
  }
}
