import Component from './Component.vue'
import store from './store'

const ComponentName = Component.name

export { Component, store, ComponentName }
export default Component
