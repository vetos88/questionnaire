/**
 * Модуль стора интернализации
*/
import {
  DATE_FORMAT,
  EMAIL_FORMAT,
  VACANCY_OFFICE,
  VACANCY_RETAIL,
  VACANCY_DEPOSITORY,
  DOCUMENT_RF_PASSPORT,
  DOCUMENT_RESIDENCE_PERMIT,
  DOCUMENT_ANOTHER_COUNTRY_PASSPORT,
  RELATIVE_DEGREE_BROTHER,
  RELATIVE_DEGREE_DAUGHTER,
  RELATIVE_DEGREE_FATHER,
  RELATIVE_DEGREE_MOTHER,
  RELATIVE_DEGREE_SISTER,
  RELATIVE_DEGREE_SON,
  RELATIVE_DEGREE_HUSBAND,
  RELATIVE_DEGREE_WIFE,
  GENDER_SIGN_FEMALE,
  GENDER_SIGN_MALE,
  MARITAL_STATUS_MARRIED,
  MARITAL_STATUS_NOT_MARRIED,
  MARITAL_STATUS_IN_DIVORCED,
  EDUCATION_TYPE_HIGHER,
  EDUCATION_TYPE_NOT_FULL_HIGHER,
  EDUCATION_TYPE_MIDDLE,
  EDUCATION_TYPE_MIDDLE_PROFESSIONAL,
  VACANCY_FINDED_PLACE_ANOTHER,
  VACANCY_FINDED_PLACE_AVITO,
  VACANCY_FINDED_PLACE_BY_EMPLOYEE,
  VACANCY_FINDED_PLACE_HH_RU,
  VACANCY_FINDED_PLACE_MAGAZINE_ADVERTISEMENT,
  VACANCY_FINDED_PLACE_NEWSPAPER_ADVERTISEMENT,
  VACANCY_FINDED_PLACE_SUPERJOB
} from './validatorsAndFormatters'

/**
 * Шаблон дя подстановки параметров
*/
const reForParams = /{}/
/**
 * TODO требуется рефакторинг для динамического формирования геттеров
*/
export const l18n = {
  // Заголовки
  personalDataCardTitle: 'ФИО',
  personalCommonDataCardTitle: 'Общая персональная информация',
  documentDataCardTitle: 'Документ удостоверяющий личность',
  passportDataCardTitle: 'Паспорт РФ',
  residencePermitDataCardTitle: 'Вид на жительство',
  anotherCountryPassportDataCardTitle: 'Паспорт иностранного гражданина',
  relativesDataCardTitle: 'Сведения о родственниках',
  educationDataCardTitle: 'Сведения об образовании',
  workingActivityDataCardTitle: 'Профессиональная деятельность',
  additionalDataCardTitle: 'Дополнительные вопросы',
  processingPersonalDataAgreementTitle: 'Даёте ли Вы согласие на обработку персональных данных?',
  // Персональные данные
  surnameLabel: 'Фамилия',
  personalSurnamePlaceholder: 'Введите Вашу фамилию',
  surnamePlaceholder: 'Введите фамилию',
  surnameHint: 'Введите фамилию',
  nameLabel: 'Имя',
  personalNamePlaceholder: 'Введите Ваше имя',
  namePlaceholder: 'Введите имя',
  nameHint: 'Введите имя',
  middleNameLabel: 'Отчество',
  personalMiddleNamePlaceHolder: 'Введите Ваше отчество',
  middleNamePlaceHolder: 'Введите отчество',
  middleNameHint: 'Введите отчество',
  genderLabel: 'Пол',
  genderPlaceholder: 'Укажите пол',
  genderSignMale: 'Мужской',
  genderSignFeMale: 'Женский',
  bornLocationLabel: 'Место рождения',
  bornLocationPlaceholder: 'Как в паспорте',
  bornLocationHint: 'Укажите место рождения как в паспорте',
  birthDayLabel: 'Дата рождения',
  birthDayPlaceholder: DATE_FORMAT,
  birthDayHint: 'Укажите дату рождения',
  liveLocationLabel: 'Место проживания',
  liveLocationPlaceholder: 'Укажите населенный пункт',
  liveLocationHint: 'Название населенного пункта',
  registeredLiveLocationLabel: 'Место регистрации',
  registeredLiveLocationPlaceholder: 'Введеный адрес должен присутствовать в списке.',
  registeredLiveLocationHint: 'Укажите место регистрации',
  sameAsRegisteredLiveLocation: 'Совпадает с местом проживания',
  workLocationLabel: 'Место работы',
  workLocationPlaceholder: 'Укажите место работы',
  workLocationHint: 'Укажите место работы',
  workPositionLabel: 'Занимаемая должность',
  workPositionPlaceholder: 'Укажите должность',
  workPositionHint: 'Укажите занимаемую должность',
  telephoneLabel: 'Номер телефона',
  telephonePlaceholder: 'Цифры без спецсимволов',
  telephoneHint: 'Укажите номер телефона',
  mobileTelephoneLabel: 'Мобильный телефон',
  mobileTelephoneHint: 'Мобильный (персональный) номер телефона',
  homeTelephoneLabel: 'Домашний телефон',
  homeTelephoneHint: 'Номер домашнего телефона',
  workTelephoneLabel: 'Рабочий телефон',
  workTelephoneHint: 'Рабочий телефон',
  emailLabel: 'Адрес электронной почты',
  emailPlaceholder: EMAIL_FORMAT,
  emailHint: 'Email',
  maritalStatusLabel: 'Семейное положение',
  maritalStatusMarriedMale: 'Женат',
  maritalStatusNotMarriedMale: 'Не женат',
  maritalStatusMarriedFeMale: 'Замужем',
  maritalStatusNotMarriedFeMale: 'Не замужем',
  maritalStatusInDivorced: 'В разводе',
  noMaritalStatusData: 'Укажите пол',
  // История места проживания
  previousLiveLocationWasLabel: 'Приходилось ли менять регион проживания?',
  addNewPreviousLiveLocationButton: 'Добавить место жительства',
  removePreviousLiveLocationButton: 'Удалить это место жительства',
  locatedPeriodLabel: 'Укажите длительность проживания',
  locatedPeriodPlaceholder: '1 год 2 месяца',
  locatedPeriodHint: 'Напишите длительность проживания',
  dateFromLabel: 'С',
  dateFromPlaceholder: DATE_FORMAT,
  dateFromHint: 'Дата начала проживания',
  dateToLabel: 'До',
  dateToPlaceholder: DATE_FORMAT,
  dateToHint: 'Дата окончания проживания',
  untilNowDateLabel: 'По настоящее время',
  // Измененные данные
  previousDataDateLabel: 'Дата изменения',
  previousDataDatePlaceholder: DATE_FORMAT,
  previousDataDateHint: 'Дата изменения данных',
  addMorePreviousData: 'Добавить еще данные',
  removePreviousDataButton: 'Удалить эти данные',
  previousPersonalDataLabel: 'Приходилось ли менять ФИО?',
  previousDataAndWhenChanged: 'Укажите все предыдущие данные и дату когда были изменены.',
  // Паспорт
  passportSeriesLabel: 'Серия паспорта',
  passportSeriesPlaceholder: 'Введите 4 цифры серии паспорта',
  passportSeriesHint: '4 цифры серии паспорта',
  passportNumberLabel: 'Номер паспорта',
  passportNumberPlaceholder: 'Введите 6 цифр номера паспорта',
  passportNumberHint: '6 цифр номера паспорта',
  passportIssueDateLabel: 'Дата выдачи паспорта',
  passportIssueDatePlaceholder: DATE_FORMAT,
  passportIssueDateHint: 'Когда выдан',
  passportIssueOrganizationLabel: 'Кем выдан',
  passportIssueOrganizationPlaceholder: 'Наименование органа выдавшего документ',
  passportIssueOrganizationHint: 'Наименование органа',
  // Вид на жительство
  residencePermitSeriesLabel: 'Серия документа',
  residencePermitSeriesPlaceholder: 'Введите серию',
  residencePermitSeriesHint: 'Серия',
  residencePermitNumberLabel: 'Номер документа',
  residencePermitNumberPlaceholder: 'Введите 7 цифр номера документа',
  residencePermitNumberHint: '7 цифр номера документа',
  residencePermitIssueDateLabel: 'Дата выдачи документа',
  residencePermitIssueDatePlaceholder: DATE_FORMAT,
  residencePermitIssueDateHint: 'Когда выдан',
  residencePermitIssueOrganizationLabel: 'Кема выдан',
  residencePermitIssueOrganizationPlaceholder: 'Наименование органа выдавшего документ',
  residencePermitIssueOrganizationHint: 'Наименование органа',
  // Паспорт иностранного гражданина
  anotherCountryPassportKindLabel: 'Название документа',
  anotherCountryPassportKindPlaceholder: 'Введите название',
  anotherCountryPassportKindHint: 'Название документа',
  anotherCountryPassportSeriesLabel: 'Серия документа',
  anotherCountryPassportSeriesPlaceholder: 'Введите серию',
  anotherCountryPassportSeriesHint: 'Серия',
  anotherCountryPassportNumberLabel: 'Номер документа',
  anotherCountryPassportNumberPlaceholder: 'Введите номер документа',
  anotherCountryPassportNumberHint: 'Номер документа',
  anotherCountryPassportIssueDateLabel: 'Дата выдачи документа',
  anotherCountryPassportIssueDatePlaceholder: DATE_FORMAT,
  anotherCountryPassportIssueDateHint: 'Когда выдан',
  anotherCountryPassportIssueOrganizationLabel: 'Кема выдан',
  anotherCountryPassportIssueOrganizationPlaceholder: 'Наименование органа выдавшего документ',
  anotherCountryPassportIssueOrganizationHint: 'Наименование органа',
  // Родственники
  addRelativeDataBlankButton: 'Добавить данные о родственнике',
  removeRelativeDataBlankButton: 'Удалить данные о родственнике',
  relativesSectionHint: 'Добавте информацию о ближайших родственниках',
  relationDegreeLabel: 'Степень родства',
  relationDegreePlaceholder: 'Укажите степень родства',
  relationDegreeHint: 'Укажите степень родства',
  isDisabledLabel: 'Имеется ли инвалидность?',
  disabledGroupGroupLabel: 'Группа инвалидности',
  disabledGroupPlaceholder: 'Укажите группу инвалидности',
  disabledGroupHint: 'Укажите группу инвалидности',
  relativeDegreeBrother: 'Брат',
  relativeDegreeSister: 'Сестра',
  relativeDegreeDaughter: 'Дочь',
  relativeDegreeSon: 'Сын',
  relativeDegreeMother: 'Мать',
  relativeDegreeFather: 'Отец',
  relativeDegreeHusband: 'Муж',
  relativeDegreeWife: 'Жена',
  // Образование
  educationLevelTypeLabel: 'Уровень образования',
  educationLevelTypePlaceholder: 'Укажите уровень образования',
  educationOrganizationTitle: 'Название образовательного учреждения',
  educationOrganizationPlaceholder: 'Укажите название',
  educationOrganizationHint: 'Название учреждения',
  specialityTitle: 'Специальность',
  specialityTilePlaceholder: 'Укажите название специальности',
  specialityHint: 'Специальность',
  qualificationLabel: 'Квалификация',
  qualificationLabelPlaceholder: 'Квалификация',
  qualificationHint: 'Квалификация',
  educationRemoveButtonTooltipLabel: 'Удалить данные об образовании',
  educationAddButtonLabel: 'Добавить сведения об образовании',
  educationTypeHigher: 'Высшее',
  educationTypeNotFullHigher: 'Не полное высшее',
  educationTypeMiddle: 'Среднее',
  educationTypeMiddleProfessional: 'Среднее-профессиональное',
  // Профессиональная деятельность
  workingActivitySectionHint: '' +
    'Места работы за последние 10 лет, начиная с последнего. Укажите реальный опыт работы, включая совместительство, ' +
    'собственный бизнес, работа по срочному договору и т.д.',
  workStartDate: 'Дата приёма на работу',
  workFinishDate: 'Дата увольнения',
  workingOrganizationTitleLabel: 'Название организации',
  workingOrganizationTitlePlaceholder: 'ООО Название Организация',
  workingOrganizationTitleHint: 'Ведите название',
  workingActivityDirectionLabel: 'Направление деятельности организации',
  workingActivityDirectionPlaceholder: 'Основные направления деятельности',
  workingActivityDirectionHint: 'Направление деятельности',
  workingPositionLabel: 'Должность',
  workingPositionPlaceholder: 'Укажите название должности',
  workingPositionHint: 'Название должности',
  subordinatesAmountLabel: 'Количество подчиненных',
  subordinatesAmountPlaceholder: 'Укажите количество',
  subordinatesAmountHint: 'Подчиненные',
  salaryLevelLabel: 'Уровень зарплаты',
  salaryLevelPlaceholder: 'Укажите уровень зарплаты',
  salaryLevelHint: 'Зарплата',
  officialDutiesLabel: 'Ваши должностные обязанности',
  officialDutiesPlaceHolder: 'Опишите Ваши должностные обязанности',
  officialDutiesHint: 'Должностные обязанности',
  firingReasonsLabel: 'Причины увольнения',
  firingReasonsPlaceholder: 'Что послужило причиной увольнения',
  firingReasonsHint: 'Причины',
  workingRemoveButtonTooltipLabel: 'Удалить эти данные',
  workingAddButtonLabel: 'Добавить данные о профессиональной деятельности',
  // Дополнительные вопросы
  pcKnowledgeLevelLabel: 'Уровень владения ПК',
  pcKnowledgeLevelPlaceholder: 'Опишите Ваш уровень владения ПК',
  pcKnowledgeLevelHint: 'Владение ПК',
  foreignLanguagesTitle: 'Иностранные языки (укажите степень владения)',
  foreignLanguageTitleLabel: 'Язык',
  foreignLanguageTitlePlaceholder: 'Название языка',
  foreignLanguageTitleHint: 'Укажите язык',
  foreignLanguageLabel: 'Уровень владения',
  foreignLanguagePlaceholder: 'Укажите свой уровень владения',
  foreignLanguageHint: 'Уровень владения',
  foreignLanguageRemoveButtonTooltipLabel: 'Удалить',
  foreignLanguageAddButtonLabel: 'Добавить Иностранный язык',
  whereFindVacancyLabel: 'Откуда Вы узнали о вакансии?',
  whereFindVacancyPlaceholder: 'Выберите один из вариантов',
  whereFindVacancyHint: 'Откуда Вы узнали о вакансии',
  checkPersonalDataAgreementByPolygraphLabel: 'Готовы ли вы, в случае необходимости, к проведению проверочных действий ' +
    '(проверка подлинности предоставленной информации, проверка на полиграфе)?',
  badHabitsHavingLabel: 'Имеются ли у Вас вредные привычки?',
  criminalConvictionAdministrativeOffensesLabel: 'Имеются ли у Вас судимости или административные правонарушения?',
  additionalNotationLabel: 'Если Вы считаете нужным что-либо добавить, сделайте это здесь.',
  additionalNotationPlaceholder: 'Примечание',
  additionalNotationHint: 'Примечание',
  salaryLevelWantedTitle: 'Сколько вы хотите зарабатывать?',
  salaryLevelWantedNowLabel: 'Сейчас',
  salaryLevelWantedNowPlaceholder: 'Укажите сумму',
  salaryLevelWantedNowHint: 'На сегодняшний день',
  salaryLevelWantedBeforeYearLabel: 'Через год',
  salaryLevelWantedBeforeYearPlaceholder: 'Укажите сумму',
  salaryLevelWantedBeforeYearHint: 'После года работы',
  salaryLevelWantedBeforeThreeYearsLabel: 'Через 3 года',
  salaryLevelWantedBeforeThreeYearsPlaceholder: 'Укажите сумму',
  salaryLevelWantedBeforeThreeYearsHint: 'После 3х лет',
  personalHobbyLabel: 'Хобби',
  personalHobbyPlaceholder: 'Опишите свои интересы и увлечения',
  personalHobbyHint: 'Ваши интересы',
  vacancyFindedPlaceHhRu: 'hh.ru',
  vacancyFindedPlaceSuperjob: 'Superjob',
  vacancyFindedPlaceAvito: 'Avito',
  vacancyFindedPlaceMagazineAdvertisement: 'Из объявления в магазине',
  vacancyFindedPlaceNewsPaperAdvertisement: 'Из объявления в газете',
  vacancyFindedPlaceByEmployee: 'От сотрудника',
  vacancyFindedPlaceAnother: 'Другое',
  // Ошибки
  fieldRequiredError: 'Поле обязательно',
  onlyLettersError: 'Только кириллица',
  onlyDigitsError: 'Только цифры',
  minLengthNotEnough: 'Не хватает символов',
  maxLengthOverhead: 'Превышено количество символов',
  dateFormatInvalid: `Ожидается дата в формате ${DATE_FORMAT}`,
  emailFormatInvalid: `Требуемый формат ${EMAIL_FORMAT}`,
  notFromListSelected: 'Выбарите пожалуйста значение из списка.',
  questionnaireFetchingErrorTemplate: 'Ошибка при получении анкеты c идентификатором "{}". ' +
    'Попробуйте начать создание анкеты сначала.'
}

/**
 * Объект стейта для динамического формирования геттеров.
*/
const additionState = {
  // Информация о вакансии
  vacancyDataCardTitleText: 'Информация о вакансии',
  vacancySelectionLabelText: 'На какую вакансию претендуете',
  vacancySelectionPlaceHolderText: 'Выберите интересующую группу',
  vacancySelectionHintText: 'Выберите интересующую вакансию',
  officeVacancyVariantTitleText: 'Вакансии офиса',
  retailVacancyVariantTitleText: 'Вакансии розницы (продавец, товаровед ломбарда) ',
  depositoryVacancyVariantTitleText: 'Вакансии склада',
  // Персональные данные
  autocompleteRefreshSuggestTooltipText: 'Если адреса нет в списке попробуйте нажать на иконку с левой стороны поля',
  citizenshipLabelText: 'Гражданство',
  citizenshipPlaceHolderText: 'Гражданство',
  citizenshipHintText: 'Гражданство',
  snilsLabelText: 'СНИЛС',
  snilsPlaceholderText: 'Введите СНИЛС',
  snilsHintText: '11 цифр СНИЛС',
  innLabelText: 'ИНН',
  innPlaceholderText: 'Введите ИНН',
  innHintText: '12 цифр СНИЛС',
  suggestionStubText: 'Начните вводить название города',
  savedFormButtonTooltipLabel: 'Форма заполнена верно. Сохранить Ваши данные.',
  savedButtonLabelText: 'Сохранить',
  noTextForDynamicSelectionText: 'Нет категорий для выбора',
  // Ошибки
  allRequiredFieldsIsNotFilledErrorText: 'У вас есть не заполненные обязательные поля',
  allRequiredFieldsIsNotFilledWarningTooltipText: 'У вас есть не заполненные обязательные поля',
  saveValidationDataErrorText: 'Ваши данные не сохранены. Попробуйте проверить все поля еще раз и повторить попытку',
  personalDataAgreementExpectedWarningTooltipText: 'Требуется согласие на обработку персональных данных',
  questionnaireDataParsingErrorText: 'Не верный формат данных от сервера.' +
    'Возможно данные были повреждены или утеряны. Выполните заполнение анкеты заново.',
  // Сообщения
  dataSuccessSavedMessageText: 'Ваши данные успешно сохранены.'
}

export const getters = {
  // Заголовки
  personalDataCardTitleText: (state) => state.l18n.personalDataCardTitle,
  personalCommonDataCardTitleText: (state) => state.l18n.personalCommonDataCardTitle,
  passportDataCardTitleText: (state) => state.l18n.passportDataCardTitle,
  documentDataCardTitleText: (state) => state.l18n.documentDataCardTitle,
  relativesDataCardTitleText: (state) => state.l18n.relativesDataCardTitle,
  processingPersonalDataAgreementTitleText: (state) => state.l18n.processingPersonalDataAgreementTitle,
  // Персональные данные
  surnameLabelText (state) {
    return state.l18n.surnameLabel
  },
  personalSurnamePlaceholderText (state) {
    return state.l18n.personalSurnamePlaceholder
  },
  surnamePlaceholderText (state) {
    return state.l18n.surnamePlaceholder
  },
  surnameHintText (state) {
    return state.l18n.surnameHint
  },
  nameLabelText (state) {
    return state.l18n.nameLabel
  },
  personalNamePlaceholderText (state) {
    return state.l18n.personalNamePlaceholder
  },
  namePlaceholderText (state) {
    return state.l18n.namePlaceholder
  },
  nameHintText (state) {
    return state.l18n.nameHint
  },
  middleNameLabelText (state) {
    return state.l18n.middleNameLabel
  },
  personalMiddleNamePlaceHolderText (state) {
    return state.l18n.personalMiddleNamePlaceHolder
  },
  middleNamePlaceHolderText (state) {
    return state.l18n.middleNamePlaceHolder
  },
  middleNameHintText (state) {
    return state.l18n.middleNameHint
  },
  genderLabelText: () => state.l18n.genderLabel,
  genderPlaceholderText: () => state.l18n.genderPlaceholder,
  bornLocationLabelText: () => state.l18n.bornLocationLabel,
  bornLocationPlaceholderText: () => state.l18n.bornLocationPlaceholder,
  bornLocationHintText: () => state.l18n.bornLocationHint,
  birthDayLabelText: () => state.l18n.birthDayLabel,
  birthDayPlaceholderText: () => state.l18n.birthDayPlaceholder,
  birthDayHintText: () => state.l18n.birthDayHint,
  liveLocationLabelText: () => state.l18n.liveLocationLabel,
  liveLocationPlaceholderText: () => state.l18n.liveLocationPlaceholder,
  liveLocationHintText: () => state.l18n.liveLocationHint,
  registeredLiveLocationLabelText: () => state.l18n.registeredLiveLocationLabel,
  registeredLiveLocationPlaceholderText: () => state.l18n.registeredLiveLocationPlaceholder,
  registeredLiveLocationHintText: () => state.l18n.registeredLiveLocationHint,
  sameAsRegisteredLiveLocationLabelText: () => state.l18n.sameAsRegisteredLiveLocation,
  workLocationLabelText: () => state.l18n.workLocationLabel,
  workLocationPlaceholderText: () => state.l18n.workLocationPlaceholder,
  workLocationHintText: () => state.l18n.workLocationHint,
  workPositionLabelText: () => state.l18n.workPositionLabel,
  workPositionPlaceholderText: () => state.l18n.workPositionPlaceholder,
  workPositionHintText: () => state.l18n.workPositionHint,
  telephoneLabelText: () => state.l18n.telephoneLabel,
  telephonePlaceholderText: () => state.l18n.telephonePlaceholder,
  telephoneHintText: () => state.l18n.telephoneHint,
  mobileTelephoneLabelText: () => state.l18n.mobileTelephoneLabel,
  mobileTelephoneHintText: () => state.l18n.mobileTelephoneHint,
  homeTelephoneLabelText: () => state.l18n.homeTelephoneLabel,
  homeTelephoneHintText: () => state.l18n.homeTelephoneHint,
  workTelephoneLabelText: () => state.l18n.workTelephoneLabel,
  workTelephoneHintText: () => state.l18n.workTelephoneHint,
  emailLabelText: () => state.l18n.emailLabel,
  emailPlaceholderText: () => state.l18n.emailPlaceholder,
  emailHintText: () => state.l18n.emailHint,
  isDisabledLabelText: () => state.l18n.isDisabledLabel,
  disabledGroupGroupLabelText: () => state.l18n.disabledGroupGroupLabel,
  disabledGroupPlaceholderText: () => state.l18n.disabledGroupPlaceholder,
  disabledGroupHintText: () => state.l18n.disabledGroupHint,
  maritalStatusLabelText: () => state.l18n.maritalStatusLabel,
  noMaritalStatusDataText: () => state.l18n.noMaritalStatusData,
  // История проживания
  previousLiveLocationWasLabelText: () => state.l18n.previousLiveLocationWasLabel,
  addNewPreviousLiveLocationButtonText: () => state.l18n.addNewPreviousLiveLocationButton,
  removePreviousLiveLocationButtonText: () => state.l18n.removePreviousLiveLocationButton,
  locatedPeriodLabelText: () => state.l18n.locatedPeriodLabel,
  locatedPeriodPlaceholderText: () => state.l18n.locatedPeriodLabel,
  locatedPeriodHintText: () => state.l18n.locatedPeriodHint,
  dateFromLabelText: () => state.l18n.dateFromLabel,
  dateFromPlaceholderText: () => state.l18n.dateFromPlaceholder,
  dateFromHintText: () => state.l18n.dateFromHint,
  dateToLabelText: () => state.l18n.dateToLabel,
  dateToPlaceholderText: () => state.l18n.dateFromPlaceholder,
  dateToHintText: () => state.l18n.dateToHint,
  // Измененные данные
  previousDataDateLabelText (state) {
    return state.l18n.previousDataDateLabel
  },
  previousDataDatePlaceholderText (state) {
    return state.l18n.previousDataDatePlaceholder
  },
  previousDataDateHintText (state) {
    return state.l18n.previousDataDateHint
  },
  previousPersonalDataLabelText (state) {
    return state.l18n.previousPersonalDataLabel
  },
  addMorePreviousDataText (state) {
    return state.l18n.addMorePreviousData
  },
  removePreviousDataButtonText: (state) => state.l18n.removePreviousDataButton,
  previousDataAndWhenChangedText (state) {
    return state.l18n.previousDataAndWhenChanged
  },
  // Паспорт
  passportSeriesLabelText () {
    return state.l18n.passportSeriesLabel
  },
  passportSeriesPlaceholderText () {
    return state.l18n.passportSeriesPlaceholder
  },
  passportSeriesHintText () {
    return state.l18n.passportSeriesHint
  },
  passportNumberLabelText () {
    return state.l18n.passportNumberLabel
  },
  passportNumberPlaceholderText () {
    return state.l18n.passportNumberPlaceholder
  },
  passportNumberHintText () {
    return state.l18n.passportNumberHint
  },
  passportIssueDateLabelText () {
    return state.l18n.passportIssueDateLabel
  },
  passportIssueDatePlaceholderText () {
    return state.l18n.passportIssueDatePlaceholder
  },
  passportIssueDateHintText () {
    return state.l18n.passportIssueDateHint
  },
  passportIssueOrganizationLabelText () {
    return state.l18n.passportIssueOrganizationLabel
  },
  passportIssueOrganizationPlaceholderText () {
    return state.l18n.passportIssueOrganizationPlaceholder
  },
  passportIssueOrganizationHintText () {
    return state.l18n.passportIssueOrganizationHint
  },
  // Вид на жительство
  residencePermitSeriesLabelText: () => state.l18n.residencePermitSeriesLabel,
  residencePermitSeriesPlaceholderText: () => state.l18n.residencePermitSeriesPlaceholder,
  residencePermitSeriesHintText: () => state.l18n.residencePermitSeriesHint,
  residencePermitNumberLabelText: () => state.l18n.residencePermitNumberLabel,
  residencePermitNumberPlaceholderText: () => state.l18n.residencePermitNumberPlaceholder,
  residencePermitNumberHintText: () => state.l18n.residencePermitNumberHint,
  residencePermitIssueDateLabelText: () => state.l18n.residencePermitIssueDateLabel,
  residencePermitIssueDatePlaceholderText: () => state.l18n.residencePermitIssueDatePlaceholder,
  residencePermitIssueDateHintText: () => state.l18n.residencePermitIssueDateHint,
  residencePermitIssueOrganizationLabelText: () => state.l18n.residencePermitIssueOrganizationLabel,
  residencePermitIssueOrganizationPlaceholderText: () => state.l18n.residencePermitIssueOrganizationPlaceholder,
  residencePermitIssueOrganizationHintText: () => state.l18n.residencePermitIssueOrganizationHint,
  // Паспорт иностранного гражданина
  anotherCountryPassportKindLabelText: () => state.l18n.anotherCountryPassportKindLabel,
  anotherCountryPassportKindPlaceholderText: () => state.l18n.anotherCountryPassportKindPlaceholder,
  anotherCountryPassportKindHintText: () => state.l18n.anotherCountryPassportKindHint,
  anotherCountryPassportSeriesLabelText: () => state.l18n.anotherCountryPassportSeriesLabel,
  anotherCountryPassportSeriesPlaceholderText: () => state.l18n.anotherCountryPassportSeriesPlaceholder,
  anotherCountryPassportSeriesHintText: () => state.l18n.anotherCountryPassportSeriesHint,
  anotherCountryPassportNumberLabelText: () => state.l18n.anotherCountryPassportNumberLabel,
  anotherCountryPassportNumberPlaceholderText: () => state.l18n.anotherCountryPassportNumberPlaceholder,
  anotherCountryPassportNumberHintText: () => state.l18n.anotherCountryPassportNumberHint,
  anotherCountryPassportIssueDateLabelText: () => state.l18n.anotherCountryPassportIssueDateLabel,
  anotherCountryPassportIssueDatePlaceholderText: () => state.l18n.anotherCountryPassportIssueDatePlaceholder,
  anotherCountryPassportIssueDateHintText: () => state.l18n.anotherCountryPassportIssueDateHint,
  anotherCountryPassportIssueOrganizationLabelText: () => state.l18n.anotherCountryPassportIssueOrganizationLabel,
  anotherCountryPassportIssueOrganizationPlaceholderText: () => state.l18n.anotherCountryPassportIssueOrganizationPlaceholder,
  anotherCountryPassportIssueOrganizationHintText: () => state.l18n.anotherCountryPassportIssueOrganizationHint,
  // Родственники
  addRelativeDataBlankButtonText: () => state.l18n.addRelativeDataBlankButton,
  removeRelativeDataBlankButtonText: () => state.l18n.removeRelativeDataBlankButton,
  relativesSectionHintText: () => state.l18n.relativesSectionHint,
  relationDegreeLabelText: () => state.l18n.relationDegreeLabel,
  relationDegreePlaceholderText: () => state.l18n.relationDegreePlaceholder,
  relationDegreeHintText: () => state.l18n.relationDegreeHint,
  // Образование
  educationDataCardTitleText: () => state.l18n.educationDataCardTitle,
  untilNowDateLabelText: () => state.l18n.untilNowDateLabel,
  educationLevelTypeLabelText: () => state.l18n.educationLevelTypeLabel,
  educationLevelTypePlaceholderText: () => state.l18n.educationLevelTypePlaceholder,
  educationOrganizationTitleText: () => state.l18n.educationOrganizationTitle,
  educationOrganizationPlaceholderText: () => state.l18n.educationOrganizationPlaceholder,
  educationOrganizationHintText: () => state.l18n.educationOrganizationHint,
  specialityTitleText: () => state.l18n.specialityTitle,
  specialityTilePlaceholderText: () => state.l18n.specialityTilePlaceholder,
  specialityHintText: () => state.l18n.specialityHint,
  qualificationLabelText: () => state.l18n.qualificationLabel,
  qualificationLabelPlaceholderText: () => state.l18n.qualificationLabelPlaceholder,
  qualificationHintText: () => state.l18n.qualificationHint,
  educationRemoveButtonTooltipLabelText: () => state.l18n.educationRemoveButtonTooltipLabel,
  educationAddButtonLabelText: () => state.l18n.educationAddButtonLabel,
  // Работа
  workingActivityDataCardTitleText: () => state.l18n.workingActivityDataCardTitle,
  workingActivitySectionHintText: () => state.l18n.workingActivitySectionHint,
  workStartDateText: () => state.l18n.workStartDate,
  workFinishDateText: () => state.l18n.workFinishDate,
  workingOrganizationTitleLabelText: () => state.l18n.workingOrganizationTitleLabel,
  workingOrganizationTitlePlaceholderText: () => state.l18n.workingOrganizationTitlePlaceholder,
  workingOrganizationTitleHintText: () => state.l18n.workingOrganizationTitleHint,
  workingActivityDirectionLabelText: () => state.l18n.workingActivityDirectionLabel,
  workingActivityDirectionPlaceholderText: () => state.l18n.workingActivityDirectionPlaceholder,
  workingActivityDirectionHintText: () => state.l18n.workingActivityDirectionHint,
  workingPositionLabelText: () => state.l18n.workingPositionLabel,
  workingPositionPlaceholderText: () => state.l18n.workingPositionPlaceholder,
  workingPositionHintText: () => state.l18n.workingPositionHint,
  subordinatesAmountLabelText: () => state.l18n.subordinatesAmountLabel,
  subordinatesAmountPlaceholderText: () => state.l18n.subordinatesAmountPlaceholder,
  subordinatesAmountHintText: () => state.l18n.subordinatesAmountHint,
  salaryLevelLabelText: () => state.l18n.salaryLevelLabel,
  salaryLevelPlaceholderText: () => state.l18n.salaryLevelPlaceholder,
  salaryLevelHintText: () => state.l18n.salaryLevelHint,
  officialDutiesLabelText: () => state.l18n.officialDutiesLabel,
  officialDutiesPlaceHolderText: () => state.l18n.officialDutiesPlaceHolder,
  officialDutiesHintText: () => state.l18n.officialDutiesHint,
  firingReasonsLabelText: () => state.l18n.firingReasonsLabel,
  firingReasonsPlaceholderText: () => state.l18n.firingReasonsPlaceholder,
  firingReasonsHintText: () => state.l18n.firingReasonsHint,
  workingRemoveButtonTooltipLabelText: () => state.l18n.workingRemoveButtonTooltipLabel,
  workingAddButtonLabelText: () => state.l18n.workingAddButtonLabel,
  // Дополнительные вопросы
  additionalDataCardTitleText: () => state.l18n.additionalDataCardTitle,
  pcKnowledgeLevelLabelText: () => state.l18n.pcKnowledgeLevelLabel,
  pcKnowledgeLevelPlaceholderText: () => state.l18n.pcKnowledgeLevelLabel,
  pcKnowledgeLevelHintText: () => state.l18n.pcKnowledgeLevelHint,
  foreignLanguagesTitleText: () => state.l18n.foreignLanguagesTitle,
  foreignLanguageTitleLabelText: () => state.l18n.foreignLanguageTitleLabel,
  foreignLanguageTitlePlaceholderText: () => state.l18n.foreignLanguageTitlePlaceholder,
  foreignLanguageTitleHintText: () => state.l18n.foreignLanguageTitleHint,
  foreignLanguageLabelText: () => state.l18n.foreignLanguageLabel,
  foreignLanguagePlaceholderText: () => state.l18n.foreignLanguagePlaceholder,
  foreignLanguageHintText: () => state.l18n.foreignLanguageHint,
  foreignLanguageRemoveButtonTooltipLabelText: () => state.l18n.foreignLanguageRemoveButtonTooltipLabel,
  foreignLanguageAddButtonLabelText: () => state.l18n.foreignLanguageAddButtonLabel,
  whereFindVacancyLabelText: () => state.l18n.whereFindVacancyLabel,
  whereFindVacancyPlaceholderText: () => state.l18n.whereFindVacancyPlaceholder,
  whereFindVacancyHintText: () => state.l18n.whereFindVacancyHint,
  checkPersonalDataAgreementByPolygraphLabelText: () => state.l18n.checkPersonalDataAgreementByPolygraphLabel,
  badHabitsHavingLabelText: () => state.l18n.badHabitsHavingLabel,
  criminalConvictionAdministrativeOffensesLabelText: () => state.l18n.criminalConvictionAdministrativeOffensesLabel,
  additionalNotationLabelText: () => state.l18n.additionalNotationLabel,
  additionalNotationPlaceholderText: () => state.l18n.additionalNotationPlaceholder,
  additionalNotationHintText: () => state.l18n.additionalNotationHint,
  salaryLevelWantedTitleText: () => state.l18n.salaryLevelWantedTitle,
  salaryLevelWantedNowLabelText: () => state.l18n.salaryLevelWantedNowLabel,
  salaryLevelWantedNowPlaceholderText: () => state.l18n.salaryLevelWantedNowPlaceholder,
  salaryLevelWantedNowHintText: () => state.l18n.salaryLevelWantedNowHint,
  salaryLevelWantedBeforeYearLabelText: () => state.l18n.salaryLevelWantedBeforeYearLabel,
  salaryLevelWantedBeforeYearPlaceholderText: () => state.l18n.salaryLevelWantedBeforeYearPlaceholder,
  salaryLevelWantedBeforeYearHintText: () => state.l18n.salaryLevelWantedBeforeYearHint,
  salaryLevelWantedBeforeThreeYearsLabelText: () => state.l18n.salaryLevelWantedBeforeThreeYearsLabel,
  salaryLevelWantedBeforeThreeYearsPlaceholderText: () => state.l18n.salaryLevelWantedBeforeThreeYearsPlaceholder,
  salaryLevelWantedBeforeThreeYearsHintText: () => state.l18n.salaryLevelWantedBeforeThreeYearsHint,
  personalHobbyLabelText: () => state.l18n.personalHobbyLabel,
  personalHobbyPlaceholderText: () => state.l18n.personalHobbyPlaceholder,
  personalHobbyHintText: () => state.l18n.personalHobbyHint,
  /**
   * Геттер возвращающий список текстов ошибок в виде обьекта
   * Ключи соответсвуют типам валидаторов vuelidate
   * https://monterail.github.io/vuelidate/#sub-builtin-validators
   * либо валидаторами в файле validatorsAndFormatters.js
  */
  textFieldErrorMessages (sate) {
    return [
      {
        validatorKey: 'required',
        errorText: state.l18n.fieldRequiredError
      },
      {
        validatorKey: 'onlyCyrillic',
        errorText: state.l18n.onlyLettersError
      },
      {
        validatorKey: 'numeric',
        errorText: state.l18n.onlyDigitsError
      },
      {
        validatorKey: 'minLength',
        errorText: state.l18n.minLengthNotEnough
      },
      {
        validatorKey: 'maxLength',
        errorText: state.l18n.maxLengthOverhead
      },
      {
        validatorKey: 'dateValueIsValid',
        errorText: state.l18n.dateFormatInvalid
      },
      {
        validatorKey: 'email',
        errorText: state.l18n.emailFormatInvalid
      },
      {
        validatorKey: 'nonSuggestedSelected',
        errorText: state.l18n.notFromListSelected
      }
    ]
  },
  /**
   * Ошибки возникающие в ходе работы приложения.
   * Тексты оформлены в виде шаблонов и предполагают наличие переменных.
  */
  questionnaireFetchingErrorText (state) {
    return brokenUuid => state.l18n.questionnaireFetchingErrorTemplate.replace(reForParams, brokenUuid)
  },
  allowedVacancySelectionsTitles (state) {
    return {
      [VACANCY_OFFICE]: state.l18n.officeVacancyVariantTitleText,
      [VACANCY_RETAIL]: state.l18n.retailVacancyVariantTitleText,
      [VACANCY_DEPOSITORY]: state.l18n.depositoryVacancyVariantTitleText
    }
  },
  allowedDocumentsTitles (state) {
    return {
      [DOCUMENT_RF_PASSPORT]: state.l18n.passportDataCardTitle,
      [DOCUMENT_RESIDENCE_PERMIT]: state.l18n.residencePermitDataCardTitle,
      [DOCUMENT_ANOTHER_COUNTRY_PASSPORT]: state.l18n.anotherCountryPassportDataCardTitle
    }
  },
  allowedRelationsDegrees (state) {
    return {
      [RELATIVE_DEGREE_HUSBAND]: state.l18n.relativeDegreeHusband,
      [RELATIVE_DEGREE_WIFE]: state.l18n.relativeDegreeWife,
      [RELATIVE_DEGREE_FATHER]: state.l18n.relativeDegreeFather,
      [RELATIVE_DEGREE_MOTHER]: state.l18n.relativeDegreeMother,
      [RELATIVE_DEGREE_BROTHER]: state.l18n.relativeDegreeBrother,
      [RELATIVE_DEGREE_SISTER]: state.l18n.relativeDegreeSister,
      [RELATIVE_DEGREE_SON]: state.l18n.relativeDegreeSon,
      [RELATIVE_DEGREE_DAUGHTER]: state.l18n.relativeDegreeDaughter
    }
  },
  allowedGenderSigns () {
    return {
      [GENDER_SIGN_MALE]: state.l18n.genderSignMale,
      [GENDER_SIGN_FEMALE]: state.l18n.genderSignFeMale
    }
  },
  maritalStatusesText () {
    return {
      [GENDER_SIGN_MALE]: {
        [MARITAL_STATUS_MARRIED]: state.l18n.maritalStatusMarriedMale,
        [MARITAL_STATUS_NOT_MARRIED]: state.l18n.maritalStatusNotMarriedMale,
        [MARITAL_STATUS_IN_DIVORCED]: state.l18n.maritalStatusInDivorced
      },
      [GENDER_SIGN_FEMALE]: {
        [MARITAL_STATUS_MARRIED]: state.l18n.maritalStatusMarriedFeMale,
        [MARITAL_STATUS_NOT_MARRIED]: state.l18n.maritalStatusNotMarriedFeMale,
        [MARITAL_STATUS_IN_DIVORCED]: state.l18n.maritalStatusInDivorced
      }
    }
  },
  allowedEducationLevelTypes () {
    return {
      [EDUCATION_TYPE_HIGHER]: state.l18n.educationTypeHigher,
      [EDUCATION_TYPE_NOT_FULL_HIGHER]: state.l18n.educationTypeNotFullHigher,
      [EDUCATION_TYPE_MIDDLE]: state.l18n.educationTypeMiddle,
      [EDUCATION_TYPE_MIDDLE_PROFESSIONAL]: state.l18n.educationTypeMiddleProfessional
    }
  },
  allowedWorkFoundedPlaces () {
    return {
      [VACANCY_FINDED_PLACE_HH_RU]: state.l18n.vacancyFindedPlaceHhRu,
      [VACANCY_FINDED_PLACE_SUPERJOB]: state.l18n.vacancyFindedPlaceSuperjob,
      [VACANCY_FINDED_PLACE_AVITO]: state.l18n.vacancyFindedPlaceAvito,
      [VACANCY_FINDED_PLACE_BY_EMPLOYEE]: state.l18n.vacancyFindedPlaceByEmployee,
      [VACANCY_FINDED_PLACE_MAGAZINE_ADVERTISEMENT]: state.l18n.vacancyFindedPlaceMagazineAdvertisement,
      [VACANCY_FINDED_PLACE_NEWSPAPER_ADVERTISEMENT]: state.l18n.vacancyFindedPlaceNewsPaperAdvertisement,
      [VACANCY_FINDED_PLACE_SUPERJOB]: state.l18n.vacancyFindedPlaceSuperjob,
      [VACANCY_FINDED_PLACE_ANOTHER]: state.l18n.vacancyFindedPlaceAnother
    }
  }
}

/**
 * Дополнительные ключи в основной стейт и формирование геттеров
*/
Object.keys(additionState).forEach(langValueKey => {
  l18n[langValueKey] = additionState[langValueKey]
  getters[langValueKey] = () => state.l18n[langValueKey]
})

/**
 * Обьект делается не изменяемым чтобы vuex не навешивал обзерверы.
*/
export const state = {
  l18n: Object.freeze(l18n)
}
