/**
 * Модели динамически добавляемых данных
*/
import {required, numeric, requiredIf} from 'vuelidate/lib/validators'
import {
  dateValueIsValid,
  dateValueIsValidIf,
  onlyCyrillic,
  RELATIVE_DEGREE_BROTHER,
  RELATIVE_DEGREE_DAUGHTER,
  RELATIVE_DEGREE_FATHER,
  RELATIVE_DEGREE_MOTHER,
  RELATIVE_DEGREE_SISTER,
  RELATIVE_DEGREE_SON,
  RELATIVE_DEGREE_HUSBAND,
  RELATIVE_DEGREE_WIFE
} from './validatorsAndFormatters'

function checkAndFormatStingValue (value) {
  return typeof value === 'string' ? value.trim() : ''
}

function checkAndFormatNumberValue (value) {
  return (!isNaN(parseInt(value, 10))) ? value : null
}

function checkAndFormatDataValue (value) {
  return checkAndFormatStingValue(value) && dateValueIsValid(value.trim()) ? value.trim() : null
}

function checkAndFormatENUMValue (value, enumCollection) {
  value = checkAndFormatStingValue(value)
  return value && (enumCollection.indexOf(value) !== -1) ? value : null
}

function checkBooleanvalue (value) {
  return typeof value === 'boolean' ? value : false
}

export class PreviousDataBlank {
  constructor ({name = '', surname = '', middleName = undefined, changeDate = null}) {
    this.name = name
    this.surname = surname
    this.middleName = middleName
    this.changeDate = changeDate
  }
  /**
   * Валидаторы формы объекта
  */
  static formValidators () {
    return {
      name: {
        required,
        onlyCyrillic
      },
      surname: {
        required,
        onlyCyrillic
      },
      middleName: {
        onlyCyrillic
      },
      changeDate: {
        required,
        dateValueIsValid
      }
    }
  }

  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      name: checkAndFormatStingValue(rawData.name),
      surname: checkAndFormatStingValue(rawData.surname),
      middleName: checkAndFormatStingValue(rawData.middleName),
      changeDate: checkAndFormatDataValue(rawData.changeDate)
    })
  }
}

const ALLOWED_RELATIONS = [
  RELATIVE_DEGREE_BROTHER,
  RELATIVE_DEGREE_DAUGHTER,
  RELATIVE_DEGREE_FATHER,
  RELATIVE_DEGREE_MOTHER,
  RELATIVE_DEGREE_SISTER,
  RELATIVE_DEGREE_SON,
  RELATIVE_DEGREE_HUSBAND,
  RELATIVE_DEGREE_WIFE
]

export class RelativeData {
  constructor ({
    name = '',
    surname = '',
    middleName = '',
    birthDay = null,
    relationDegree = null, // ENUM смотреть validatorsAndFormatters Доступные степени родства
    bornLocation = '',
    liveLocation = '',
    workLocation = '',
    workPosition = '',
    telephone = null,
    isDisabled = false,
    disabledGroup = ''
  }) {
    this.name = name
    this.surname = surname
    this.middleName = middleName
    this.birthDay = birthDay
    this.relationDegree = relationDegree
    this.bornLocation = bornLocation
    this.liveLocation = liveLocation
    this.workLocation = workLocation
    this.workPosition = workPosition
    this.telephone = telephone
    this.isDisabled = isDisabled
    this.disabledGroup = disabledGroup
  }

  static formValidators () {
    return {
      name: {
        required,
        onlyCyrillic
      },
      surname: {
        required,
        onlyCyrillic
      },
      middleName: {
        onlyCyrillic
      },
      birthDay: {
        required,
        dateValueIsValid
      },
      relationDegree: {
        required
      },
      bornLocation: {
        required
      },
      liveLocation: {
        required
      },
      workLocation: {
        required
      },
      workPosition: {
        required
      },
      telephone: {
        required,
        numeric
      },
      isDisabled: {
        // Поле не валидиркется добавлено чтобы не делать дополнительные проверки на наличие поля
      },
      disabledGroup: {
        required: requiredIf(function (relativeData) {
          return relativeData.isDisabled
        })
      }
    }
  }

  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      name: checkAndFormatStingValue(rawData.name),
      surname: checkAndFormatStingValue(rawData.surname),
      middleName: checkAndFormatStingValue(rawData.middleName),
      birthDay: checkAndFormatDataValue(rawData.birthDay),
      relationDegree: checkAndFormatENUMValue(rawData.relationDegree, ALLOWED_RELATIONS),
      bornLocation: checkAndFormatStingValue(rawData.bornLocation),
      liveLocation: checkAndFormatStingValue(rawData.liveLocation),
      workLocation: checkAndFormatStingValue(rawData.workLocation),
      workPosition: checkAndFormatStingValue(rawData.workPosition),
      telephone: checkAndFormatNumberValue(rawData.telephone),
      isDisabled: checkBooleanvalue(rawData.isDisabled),
      disabledGroup: checkAndFormatStingValue(rawData.disabledGroup)
    })
  }
}

export class PreviousLiveLocation {
  constructor ({
    liveLocation = '',
    dateFrom = null,
    dateTo = null
  }) {
    this.liveLocation = liveLocation
    this.dateFrom = dateFrom
    this.dateTo = dateTo
  }

  static formValidators () {
    return {
      liveLocation: {
        // required
      },
      dateFrom: {
        // required,
        // dateValueIsValid
      },
      dateTo: {
        // required,
        // dateValueIsValid
      }
    }
  }
  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      liveLocation: checkAndFormatStingValue(rawData.liveLocation),
      dateFrom: checkAndFormatDataValue(rawData.dateFrom),
      dateTo: checkAndFormatDataValue(rawData.dateTo)
    })
  }
}

// const ALLOVED_EDUCATIONS_TYPES = [
//   EDUCATION_TYPE_HIGHER,
//   EDUCATION_TYPE_NOT_FULL_HIGHER,
//   EDUCATION_TYPE_MIDDLE,
//   EDUCATION_TYPE_MIDDLE_PROFESSIONAL
// ]
export class EducationalInstitution {
  constructor ({
    dateFrom = null,
    untilNow = false,
    dateTo = null,
    educationLevelType = null,
    institutionName = '',
    specialty = '',
    qualification = ''
  }) {
    this.dateFrom = dateFrom
    this.untilNow = untilNow
    this.dateTo = dateTo
    this.educationLevelType = educationLevelType
    this.institutionName = institutionName
    this.specialty = specialty
    this.qualification = qualification
  }
  static formValidators () {
    return {
      dateFrom: {
        required,
        dateValueIsValid
      },
      untilNow: {
      },
      dateTo: {
        required: requiredIf(function (educationData) {
          return !educationData.untilNow
        }),
        dateValueIsValid: dateValueIsValidIf(function (educationData) {
          return !educationData.untilNow
        })
      },
      educationLevelType: {
        required
      },
      institutionName: {
        required
      },
      specialty: {
        required
      },
      qualification: {
        required
      }
    }
  }
  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      dateFrom: checkAndFormatDataValue(rawData.dateFrom),
      untilNow: checkBooleanvalue(rawData.untilNow),
      dateTo: checkAndFormatDataValue(rawData.dateTo),
      educationLevelType: checkAndFormatStingValue(rawData.educationLevelType),
      institutionName: checkAndFormatStingValue(rawData.institutionName),
      specialty: checkAndFormatStingValue(rawData.specialty),
      qualification: checkAndFormatStingValue(rawData.qualification)
    })
  }
}

export class WorkingOrganization {
  constructor ({
    dateFrom = null,
    untilNow = false,
    dateTo = null,
    title = '',
    activityDirection = '',
    position = '',
    officialDuties = '', // Должностные обязанноти
    subordinatesAmount = null, // Количество подчиненных
    salaryLevel = null, // Уровень ЗП
    firingReasons = '' // Причины увольнения
  }) {
    this.dateFrom = dateFrom
    this.untilNow = untilNow
    this.dateTo = dateTo
    this.title = title
    this.activityDirection = activityDirection
    this.position = position
    this.officialDuties = officialDuties
    this.subordinatesAmount = subordinatesAmount
    this.salaryLevel = salaryLevel
    this.firingReasons = firingReasons
  }
  static formValidators () {
    return {
      dateFrom: {
        required,
        dateValueIsValid
      },
      untilNow: {
      },
      dateTo: {
        required: requiredIf(function (workingOrganozation) {
          return !workingOrganozation.untilNow
        }),
        dateValueIsValid: dateValueIsValidIf(function (workingOrganozation) {
          return !workingOrganozation.untilNow
        })
      },
      title: {
        required
      },
      activityDirection: {
        required
      },
      position: {
        required
      },
      officialDuties: {
        required
      },
      subordinatesAmount: {
        numeric
      },
      salaryLevel: {},
      firingReasons: {
        required: requiredIf(function (relativeData) {
          return !relativeData.untilNow
        })
      }
    }
  }
  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      dateFrom: checkAndFormatDataValue(rawData.dateFrom),
      untilNow: checkBooleanvalue(rawData.untilNow),
      dateTo: checkAndFormatDataValue(rawData.dateTo),
      title: checkAndFormatStingValue(rawData.title),
      activityDirection: checkAndFormatStingValue(rawData.activityDirection),
      position: checkAndFormatStingValue(rawData.position),
      officialDuties: checkAndFormatStingValue(rawData.officialDuties),
      subordinatesAmount: checkAndFormatNumberValue(rawData.subordinatesAmount),
      salaryLevel: checkAndFormatNumberValue(rawData.salaryLevel),
      firingReasons: checkAndFormatStingValue(rawData.firingReasons)
    })
  }
}

export class ForeignLanguage {
  constructor ({
    title = '',
    level = null
  }) {
    this.title = title
    this.level = level
  }

  static formValidators () {
    return {
      title: {
        required
      },
      level: {
        required
      }
    }
  }
  /**
   * Использован паттер фабрики для очистки и преобразования данных
   * @param rawData - обьект неопределенной структуры полученный от севера
   */
  static createObject (rawData) {
    if (!rawData || typeof rawData !== 'object') {
      return null
    }
    return new this({
      title: checkAndFormatStingValue(rawData.title),
      level: checkAndFormatStingValue(rawData.level)
    })
  }
}
