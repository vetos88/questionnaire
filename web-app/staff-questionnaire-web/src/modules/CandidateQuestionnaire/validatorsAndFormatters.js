/**
 * Кастомные валидаторы полей формы
*/

import * as moment from 'moment'
import { helpers } from 'vuelidate/lib/validators'

/**
 * Валидатор того что строка содержит только кирилические символы
*/
export const onlyCyrillic = (value) => /^[а-яА-ЯЁё-]+$/.test(value) || !value

/**
 *
*/
export const DATE_FORMAT = 'YYYY-MM-DD'
export const EMAIL_FORMAT = 'example@site.domain'
export const MIN_AGE_OF_CANDIDATE = 18

export const dateValueIsValid = (value) => moment(value, DATE_FORMAT, true).isValid()
export const dateValueIsValidIf = (prop) => helpers.withParams({type: 'dateValueIsValidIf', prop},
  function (value, parentVm) {
    console.debug('dateValueIsValidIf value, parentVm', value, parentVm)
    return helpers.ref(prop, this, parentVm) ? moment(value, DATE_FORMAT, true).isValid() : true
  })
export const isMore18YearsOld = (value) => MIN_AGE_OF_CANDIDATE >= moment().diff(moment(value), 'years')
export const nonSuggestedSelectedIf = (notInList) => helpers.withParams(
  {type: 'dateValueIsValidIf', notInList},
  function (value, parentVm) {
    console.debug('dateValueIsValidIf value, parentVm', value, parentVm)
    return helpers.ref(notInList, this, parentVm)
  })
/**
 * Форматтер преобразования первого символа строки в заглвный
*/

export const capitalize = (value) => value.charAt(0).toUpperCase() + value.slice(1).toLocaleLowerCase()

/**
 * Валидатор того, что пользователь не изменял ввод после выбора предложенных вариантов
*/

export function receivedSuggestionContainsUserInput (value) {
  console.warn('receivedSuggestionContainsUserInput this', this)
  console.warn('receivedSuggestionContainsUserInput value', value)
  return this
}

/**
 * Константы возможных вариантов предлагаемых вакансий.
*/

export const VACANCY_OFFICE = 'VACANCY_OFFICE'
export const VACANCY_RETAIL = 'VACANCY_RETAIL'
export const VACANCY_DEPOSITORY = 'VACANCY_DEPOSITORY'

/**
 * Константы возможных персональных документов.
*/
export const DOCUMENT_RF_PASSPORT = 'RF_PASSPORT'
export const DOCUMENT_RESIDENCE_PERMIT = 'RESIDENCE_PERMIT'
export const DOCUMENT_ANOTHER_COUNTRY_PASSPORT = 'ANOTHER_COUNTRY_PASSPORT'

/**
 * Доступные степени родства
*/
export const RELATIVE_DEGREE_HUSBAND = 'RELATIVE_DEGREE_HUSBAND'
export const RELATIVE_DEGREE_WIFE = 'RELATIVE_DEGREE_WIFE'
export const RELATIVE_DEGREE_SON = 'RELATIVE_DEGREE_SON'
export const RELATIVE_DEGREE_DAUGHTER = 'RELATIVE_DEGREE_DAUGHTER'
export const RELATIVE_DEGREE_BROTHER = 'RELATIVE_DEGREE_BROTHER'
export const RELATIVE_DEGREE_SISTER = 'RELATIVE_DEGREE_SISTER'
export const RELATIVE_DEGREE_MOTHER = 'RELATIVE_DEGREE_MOTHER'
export const RELATIVE_DEGREE_FATHER = 'RELATIVE_DEGREE_FATHER'

/**
 * Пол
*/
export const GENDER_SIGN_MALE = 'GENDER_SIGN_MALE'
export const GENDER_SIGN_FEMALE = 'GENDER_SIGN_FEMALE'

/**
 * Семейное положение
*/
export const MARITAL_STATUS_MARRIED = 'MARITAL_STATUS_MARRIED'
export const MARITAL_STATUS_NOT_MARRIED = 'MARITAL_STATUS_NOT_MARRIED'
export const MARITAL_STATUS_IN_DIVORCED = 'MARITAL_STATUS_IN_DIVORCED'
/**
 * Образование
*/
export const EDUCATION_TYPE_HIGHER = 'EDUCATION_TYPE_HIGHER'
export const EDUCATION_TYPE_NOT_FULL_HIGHER = 'EDUCATION_TYPE_NOT_FULL_HIGHER'
export const EDUCATION_TYPE_MIDDLE = 'EDUCATION_TYPE_MIDDLE'
export const EDUCATION_TYPE_MIDDLE_PROFESSIONAL = 'EDUCATION_TYPE_MIDDLE_PROFESSINOAL'

/**
 * Ресурсы информации о вакансии
*/
export const VACANCY_FINDED_PLACE_HH_RU = 'VACANCY_FINDED_PLACE_HH_RU'
export const VACANCY_FINDED_PLACE_SUPERJOB = 'VACANCY_FINDED_PLACE_SUPERJOB'
export const VACANCY_FINDED_PLACE_AVITO = 'VACANCY_FINDED_PLACE_AVITO'
export const VACANCY_FINDED_PLACE_NEWSPAPER_ADVERTISEMENT = 'VACANCY_FINDED_PLACE_NEWSPAPER_ADVERTISEMENT'
export const VACANCY_FINDED_PLACE_MAGAZINE_ADVERTISEMENT = 'VACANCY_FINDED_PLACE_MAGAZINE_ADVERTISEMENT'
export const VACANCY_FINDED_PLACE_BY_EMPLOYEE = 'VACANCY_FINDED_PLACE_BY_EMPLOYEE'
export const VACANCY_FINDED_PLACE_ANOTHER = 'VACANCY_FINDED_PLACE_ANOTHER'

/**
 * Динамически задаваемые варианты выбора
*/
export const DYNAMIC_SELECTION_EDUCATIONS = 'Educations'
export const DYNAMIC_SELECTION_CITIZENSHIP = 'Citizenship'
export const DYNAMIC_SELECTION_FOREIGN_LANGUAGES = 'ForeignLanguages'
export const DYNAMIC_SELECTION_FOREIGN_FOREIGN_LANGUAGES_LEVELS = 'ForeignLanguagesLevels'
export const DYNAMIC_SELECTION_INTERESTS = 'Interests'
export const DYNAMIC_SELECTION_RELATION_DEGREES = 'RelationDegrees'
