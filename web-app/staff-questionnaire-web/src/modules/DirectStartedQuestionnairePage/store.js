/**
 * Модуль стора компонента
 */
import Api from '@/services/api'

/**
 * Обьекст с переменными интернационализации
*/
const l18n = {
  startQuestionnaireButtonLabelText: 'Нажмите для начала заполнения анкеты',
  getUuidFailedText: 'Не удалось создать новую анкету'
}

/**
 * Обьект для генерации гетеров
*/
const autoGetters = {}

/**
 * Дополнительные ключи в основной стейт и формирование геттеров
*/
Object.keys(l18n).forEach(langValueKey => {
  autoGetters[langValueKey] = (state) => state.l18n[langValueKey]
})

/**
 * Стор компонента
*/
export default {
  state: {
    l18n: Object.freeze(l18n)
  },
  mutations: {
  },
  actions: {
    async getNewQuestionnaireId ({commit, getters, dispatch}) {
      commit('setLoading', true)
      try {
        const data = await Api.getNewQuestionnaireId()
        console.debug('saveQuestionnaireData response from server APi data', data)
        dispatch('setQuestionnaireUuid', data) // может выбросить ошибку валидации данных
      } catch (error) {
        console.warn('getNewQuestionnaireId', error)
        commit('setError', getters.getUuidFailedText)
        throw new Error('No valid data')
      } finally {
        commit('setLoading', false)
      }
    }
  },
  getters: {
    ...autoGetters
  }
}
