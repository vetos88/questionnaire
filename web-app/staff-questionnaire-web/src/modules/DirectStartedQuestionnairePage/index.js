import Component from './Component.vue'
import store from './store'

export { Component, store }
export default Component
