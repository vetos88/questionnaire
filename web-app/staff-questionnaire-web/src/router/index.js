import Router from 'vue-router'
import CandidateQuestionnaire from '@/modules/CandidateQuestionnaire'
import DirectStartedQuestionnairePage from '@/modules/DirectStartedQuestionnairePage'
import EmailStartedQuestionnairePage from '@/modules/EmailStartedQuestionnairePage'
import MainLayout from '@/modules/MainLayout'
import {Component as NotFound} from '@/modules/NotFound'
import * as guards from './guards'

export default new Router({
  routes: [
    {
      path: '/',
      component: MainLayout,
      children: [
        {
          path: '',
          name: 'StartedQuestionnairePage',
          beforeEnter: guards.redirectToStartPage
        },
        {
          path: 'direct-start',
          name: DirectStartedQuestionnairePage.name,
          component: DirectStartedQuestionnairePage,
          beforeEnter: guards.internalAccessControl
        },
        {
          path: 'email-start',
          name: EmailStartedQuestionnairePage.name,
          component: EmailStartedQuestionnairePage
        },
        {
          path: 'candidate-questionnaire/:uuid',
          name: `${CandidateQuestionnaire.name}-uuid`,
          component: CandidateQuestionnaire
        }
      ]
    },
    {
      path: '*',
      name: NotFound.name,
      component: NotFound
    }
  ]
})
