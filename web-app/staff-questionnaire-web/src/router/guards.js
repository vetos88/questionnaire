/**
 * Функциии контроля маршрутов
 * */

/**
 * Функция перенаправлен на стартовую страницу в зависимости от хоста,
 * с которого был выполнен запрос.
*/
export function redirectToStartPage (from, to, next) {
  if (window.location.hostname.indexOf(process.env.SELF_SERVICE_INTERNAL_ACCESS_MARKER) === -1) {
    next({name: 'EmailStartedQuestionnairePage'})
  } else {
    next({name: 'DirectStartedQuestionnairePage'})
  }
}

/**
 * Гуард контроля доступа из внутренней сети.
 * Доступ к роутам предоставляеться только из внутренней сети.
 * Макер внутренней сети указаваться в файлах настройки для сборки.
*/
export function internalAccessControl (from, to, next) {
  if (window.location.hostname.indexOf(process.env.SELF_SERVICE_INTERNAL_ACCESS_MARKER) === -1) {
    next({name: 'StartedQuestionnairePage'})
  } else {
    next()
  }
}
