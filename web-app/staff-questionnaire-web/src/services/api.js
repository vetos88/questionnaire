/**
 * Сервис доступа к апи
 */

/**
* Фабрика клиента api.
*/
import axios from 'axios'

import { DEV_ENVIRONMENT } from 'config&/consts'
export const backendEndPoints = {
  getNewQuestionnaireId: {method: 'get', url: 'api/v1/launch.getNewQuestionnaire'},
  saveQuestionnaireData: {method: 'post', url: 'api/v1/user.saveData'},
  getQuestionnaireData: {method: 'get', url: 'api/v1/user.getData'},
  getAddressSuggestions: {method: 'post', url: 'api/v1/suggestion.getAddress'},
  getSelectionsCollections: {method: 'get', url: 'api/v1/collection.getAll'},
  getQuestionnaireLinkOnEmail: {method: 'post', url: 'api/v1/launch.getNewQuestionnaireLink'}
}

// Назначение базового урл для выполнения запроса к АПИ.
  // Предполагеается, что в режиме разработки бекенд имеет отдельный вебсервер.
const BACKEND_SERVER = (() => {
  if (process.env.NODE_ENV === DEV_ENVIRONMENT) {
    const backendServer = 'http://localhost:8080'
    console.warn('NODE_ENV', process.env.NODE_ENV)
    console.warn('Backend server', backendServer)
    return backendServer
  }
  return ''
})()
/**
* Фабрика создает объект-обертку над клиентом.
*/
class Api {
  constructor (options = {}) {
    this.defaultHeaders = options.headers || {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    // Создание экземпляра клиента.
    this.client = options.client ||
       axios.create({
         baseURL: BACKEND_SERVER,
         headers: this.defaultHeaders
       })
  }
  /**
   * Получение идентификатора для создания новой анкеты
  */
  getNewQuestionnaireId () {
    console.debug('getNewQuestionnaireId')
    return this.client({
      ...backendEndPoints.getNewQuestionnaireId
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getNewQuestionnaireId response data', data)
        console.debug('getNewQuestionnaireId response error', error)
        if (error || !data) {
          console.warn('Server backendEndPoints.getNewQuestionnaireId', backendEndPoints.getNewQuestionnaireId)
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
  }
  /**
   * Отправка пользовательских данных анкете в формате JSON
   */
  saveQuestionnaireData (userUuid, sentUserData) {
    console.debug('sentUserData sentUserData', sentUserData)
    return this.client({
      ...backendEndPoints.saveQuestionnaireData,
      ...{
        data: {
          scheme: sentUserData,
          uuid: userUuid
        }
      }
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getNewQuestionnaireId response data', data)
        console.debug('getNewQuestionnaireId response error', error)
        if (error || !data) {
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
      .then(({ data }) => data)
  }

  /**
   * Получение пользовательcких данных по uuid
  */
  getQuestionnaireData (userUuid) {
    console.debug('getQuestionnaireData userUuid', userUuid)
    return this.client({
      ...backendEndPoints.getQuestionnaireData,
      ...{
        params: {
          uuid: userUuid
        }
      }
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getNewQuestionnaireId response data', data)
        console.debug('getNewQuestionnaireId response error', error)
        if (error || !data) {
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
  }
  /**
   * Подсказки адреса
  */
  getAddressesSuggestions (userUuid, userPhrase) {
    console.debug('getAddressesSuggestion userUuid', userUuid)
    console.debug('getAddressesSuggestion userPhrase', userPhrase)
    return this.client({
      ...backendEndPoints.getAddressSuggestions,
      ...{
        params: {
          uuid: userUuid,
          query: userPhrase
        }
      }
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getNewQuestionnaireId response data', data)
        console.debug('getNewQuestionnaireId response error', error)
        if (error || !data) {
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
  }
  /**
   * Предуставновленные варианты выбора предустановленные из внешних систем
  */
  getExternalSelectionsCollections (userUuid) {
    console.debug('getExternalSelectionsCollections userUuid', userUuid)
    return this.client({
      ...backendEndPoints.getSelectionsCollections,
      ...{
        params: {
          uuid: userUuid
        }
      }
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getExternalSelectionsCollections response data', data)
        console.debug('getExternalSelectionsCollections response error', error)
        if (error || !data) {
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
  }
  /**
   * Запрос на создание ссылки для анкеты на емаил.
  */
  getNewQuestionnaireLinkByEmail (email) {
    console.debug('getNewQuestionnaireLinkByEmail email', email)
    return this.client({
      ...backendEndPoints.getQuestionnaireLinkOnEmail,
      ...{
        data: {
          email: email
        }
      }
    })
      .then(({data}) => data)
      .then(({ data, error }) => {
        console.debug('getNewQuestionnaireLinkByEmail response data', data)
        console.debug('getNewQuestionnaireLinkByEmail response error', error)
        if (error || !data) {
          console.error('Server error', error)
          throw new Error('Server error')
        }
        return data
      })
  }
}

export default new Api()
