import Vuex from 'vuex'
import shared from './shared'
import Vue from 'vue'
import { store as notFound } from '@/modules/NotFound'
import { store as MainLayout } from '@/modules/MainLayout'
import { store as candidateQuestionnaire } from '@/modules/CandidateQuestionnaire'
import { store as directStartedQuestionnairePage } from '@/modules/DirectStartedQuestionnairePage'
import { store as emailStartedQuestionnairePage } from '@/modules/EmailStartedQuestionnairePage'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    shared,
    notFound,
    MainLayout,
    candidateQuestionnaire,
    directStartedQuestionnairePage,
    emailStartedQuestionnairePage,
    envSettings: {
      state: Object.freeze({
        NODE_ENV: process.env.NODE_ENV,
        SELF_SERVICE_INTERNAL_ACCESS_MARKER: process.env.SELF_SERVICE_INTERNAL_ACCESS_MARKER
      })
    }
  }
})
