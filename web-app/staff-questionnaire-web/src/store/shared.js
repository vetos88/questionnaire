/**
* Модуль стейта общей функциональности.
*/

const ERROR_LEVELS = {
  WARN: 'warningUserMessage',
  ERR: 'errorUserMessage',
  SUCCESS: 'successUserMessage'
}

const ERROR_BAR_COLOR = {
  [ERROR_LEVELS.WARN]: 'warning',
  [ERROR_LEVELS.ERR]: 'error',
  [ERROR_LEVELS.SUCCESS]: 'success'
}

export default {
  state: {
    isLoading: false,
    errorMessage: null,
    errorLevels: ERROR_LEVELS,
    errorLevel: ERROR_LEVELS.ERR
  },
  mutations: {
    setLoading (state, payload) {
      state.isLoading = payload
    },
    setError (state, payload) {
      state.errorMessage = payload
    },
    setLevel (state, payload) {
      state.errorLevel = payload || ERROR_LEVELS.ERR
    },
    clearError (state) {
      state.errorMessage = null
    },
    toggleDrawer (state) {
      state.drawer = !state.drawer
    },
    setDrawerState (state, payload) {
      state.drawer = payload
    },
    changeServiceAddingDialogState (state, payload) {
      state.serviceAddingDialog = payload
    }
  },
  actions: {
    setLoading ({commit}, payload) {
      commit('setLoading', payload)
    },
    setError ({commit}, payload) {
      console.debug('setError payload', payload)
      if (typeof payload === 'object') {
        commit('setLevel', payload.level)
        commit('setError', payload.message)
      } else {
        commit('setLevel')
        commit('setError', payload)
      }
    },
    clearError ({commit}) {
      commit('clearError')
    },
    toggleDrawer ({commit}) {
      commit('toggleDrawer')
    },
    setDrawerState ({commit, state}, payload) {
      if (state.drawer !== payload) {
        commit('toggleDrawer', payload)
      }
    },
    setServiceAddingDialogState ({commit, state}, payload) {
      if (state.serviceAddingDialog !== payload) {
        commit('changeServiceAddingDialogState', payload)
      }
    }
  },
  getters: {
    loadingState (state) {
      return state.isLoading
    },
    errorMessage (state) {
      return state.errorMessage
    },
    errorLevelColor (state) {
      return ERROR_BAR_COLOR[state.errorLevel]
    },
    errorLevels (state) {
      return state.errorLevels
    }
  }
}
