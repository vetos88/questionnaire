import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Router from 'vue-router'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.red,  // #F44336
    secondary: colors.yellow.darken4,
    accent: colors.indigo.darken1,
    error: colors.red.accent4,
    warning: colors.yellow.darken3,
    info: colors.blue,
    success: colors.lightGreen.darken3,
    documentSliderHeaderColor: colors.blueGrey.lighten4
  }
})
Vue.use(Router)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
