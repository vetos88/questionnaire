#!/usr/bin/env bash
# Менеджер управления процесами сборки и работы с репозиторием.
# Для работы с репозиторием необходимо наличие файла build_env. Пример переменных в файле build_env.example.
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

# Именование образов
WEB_APP_IMAGE="web-app"
AIOHTTP_API_IMAGE="aiohttp_api"
LOOP_PUSH_IMAGE="loop_push"

# Список доступных команд. Команда передается первым аргументом.
commands=(
"help"
"build-all"
${WEB_APP_IMAGE}
${AIOHTTP_API_IMAGE}
${LOOP_PUSH_IMAGE}
)

set -evx

# Директории проектов
AIOHTTP_DIR="$(pwd)/aiohttp_api"
WEB_APP_DIR="$(pwd)/web-app"
LOOP_PUSH_DIR="$(pwd)/loop_push"

# Имя проекта
PROJECT_NAME="questionnaire"

# Meta
GIT_COMMIT=$(git rev-parse HEAD)
GIT_COMMIT_SHORT=$(git rev-parse --short HEAD)
GIT_BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
TAG_VERSION=$(git describe --tags --abbrev=0 ${GIT_COMMIT})

# Namespace
NAMESPSACE_TAG="hr"

# Настройки инфраструктуры
registry_host="registry.zoloto585.int:10443"

# Словрь образов
declare -A IMAGES=(
    [${AIOHTTP_API_IMAGE}]=${AIOHTTP_DIR}
    [${WEB_APP_IMAGE}]=${WEB_APP_DIR}
    [${LOOP_PUSH_IMAGE}]=${LOOP_PUSH_DIR}
)

function build_image {
    # Функция-сборщик образов. Принимает два аргумента
    # $1 - тег образа
    # $2 - абсолютный путь папки исходников и докефайла
    echo "Building $1";
    docker build --file "$2/Dockerfile" \
     --tag $1 \
     --build-arg GIT_COMMIT=${GIT_COMMIT} \
     --build-arg GIT_BRANCH=${GIT_BRANCH} \
     --compress \
     --no-cache \
     $2
}

function push_image_to_repo {
    # Отправление образов в регистри.
    # $1 - имя образа
    if [[ -z "$2" ]]; then
        curr_version=${BUILD_VERSION:-$TAG_VERSION}
        echo -e "Please set version for publishing in registry (current: ${GREEN}${curr_version}${NC})"
        read user_version
        curr_version=${user_version:-$curr_version}
    else
        curr_version=$2
    fi
    image_local_tag=${PROJECT_NAME}/$1:${GIT_BRANCH}-${GIT_COMMIT_SHORT}
    registry_image_tag=${registry_host}/${PROJECT_NAME}/$1:${curr_version}
    docker tag ${image_local_tag} ${registry_image_tag}
    docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASS} ${registry_host}
    echo "Pushing $registry_image_tag";
    docker push ${registry_image_tag}
    echo -e "Image ${YELLOW}${registry_image_tag}${NC} in repo!"
    #TODO поэксперимиентировать удалять или нет локальный образ в случаем помещения в репозиторий
    docker rmi ${image_local_tag}
    docker rmi ${registry_image_tag}
}


if [[ " ${commands[@]} " =~ " $1 " ]]; then
    if [[ "$1" == ${commands[0]} ]]; then
    # Список всех команд.
        echo -e "${GREEN}Аvaliable commands:${NC}"
        echo ${commands[*]}
    else
        echo -e "${GREEN}$1 - command was provided.${NC}"
        if [ -f build_env  ]; then
            source ./build_env
        else
            echo -e "${YELLOW} file with name${NC} build_env ${YELLOW}not found!${NC} custom settings not provided. Use default params.
        ${YELLOW}Registry may not be available!${NC}"
        fi
        registry_host=${CUSTOM_REGISTRY:-$registry_host}
        if [[ "$1" == ${commands[1]} ]]; then
            for image_name in "${!IMAGES[@]}"; do
                (
                    build_image ${PROJECT_NAME}/${image_name}:${GIT_BRANCH}-${GIT_COMMIT_SHORT} ${IMAGES[$image_name]}
                ) &
            done
            wait
            for image_name in "${!IMAGES[@]}"; do
                echo -e "Image with tag ${GREEN} ${PROJECT_NAME}/${image_name}:${GIT_BRANCH}-${GIT_COMMIT_SHORT} ${NC} was created!"
            done
            for (( ; ; ))
            do
                echo -e "Do you want push images to repo(images will remove locally)? registry: ${YELLOW} ${registry_host} ${NC}(Y/n)(n)"
                read user_decision
                user_decision=${user_decision:-"n"}
                if [[ "$user_decision" == "Y" || "$user_decision" == "n" ]]; then
                    break
                else
                    echo -e "${RED}Y or n only!${NC}"
                fi
            done
            if [[ "$user_decision" == "Y" ]]; then
                curr_version=${BUILD_VERSION:-$TAG_VERSION}
                echo -e "Please set version for publishing in registry (current: ${GREEN}${curr_version}${NC})"
                read user_version
                curr_version=${user_version:-$curr_version}
                for image_name in "${!IMAGES[@]}"; do
                    push_image_to_repo $image_name $curr_version
                done
            else
                docker images | grep ${PROJECT_NAME}
            fi
        else
            echo -e "${GREEN}New image will build.${NC}"
            # формируем базовое имя для создаваемого образа
            image_local_tag=${PROJECT_NAME}/$1:${GIT_BRANCH}-${GIT_COMMIT_SHORT}
            # Выполняем сборку
            build_image ${image_local_tag} ${IMAGES[$1]}

            echo -e "Image with tag ${GREEN} $1:${GIT_BRANCH}-${GIT_COMMIT_SHORT} ${NC} was created!"
            for (( ; ; ))
            do
                echo -e "Do you want push image to repo(image will remove locally)? registry: ${YELLOW} ${registry_host} ${NC}(Y/n)(n)"
                read user_decision
                user_decision=${user_decision:-"n"}
                if [[ "$user_decision" == "Y" || "$user_decision" == "n" ]]; then
                    break
                else
                    echo -e "${RED}Y or n only!${NC}"
                fi
            done
            if [[ "$user_decision" == "Y" ]]; then
                push_image_to_repo $1
            else
                docker images | grep ${PROJECT_NAME}
            fi
            echo -e "${GREEN}Build complete! ${NC}"
        fi
    fi
else
    echo -e "${RED}$0 $1 - Bad command provided!${NC} ${YELLOW}Provide one of: [${commands[*]}]. ${NC}"
fi
