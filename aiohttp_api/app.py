#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

import aiohttp_jinja2
import aioredis
import asyncio
import argparse
import logging

import jinja2
import pytest
from aiohttp_swagger import setup_swagger
from aiohttp import web
from raven import Client as Sentry

import config
import routes
import utils

# Подключение шаблонов
# Получение рабочей директории.
from utils.mailer_adapter import MailerServiceAdapter

WORK_DIR = os.getcwd()
# Определение папки щаблонов
TEMPLATE_FOLDER = os.path.join(WORK_DIR, 'templates')


async def create_app(is_debug_enabled: bool = False) -> web.Application:
    """
    Factory function creates
    web application using aiohttp
    framework.
    :param is_debug_enabled: enable aiohttp_debug
    :return: Application
    """

    # Enabling local logging
    logging.basicConfig(
        **config.Config.logging()
    )
    logging.warning('DEBUG is {} on server!'.format('ON' if is_debug_enabled > 0 else 'OFF'))

    # Initialize logger
    logger = logging.getLogger('aiohttp.access')

    # Initialize Sentry
    sentry = Sentry(**config.Config.sentry())

    # Initialization web server
    if is_debug_enabled:
        api = web.Application()
        api.router.add_routes(routes.ROUTE_TABLE)

        # Initialize redis
        api['redis'] = await aioredis.create_redis_pool(**config.Config.redis())
        api.on_shutdown.append(utils.signals.close_redis_connection)
        api['mailer_adapter'] = MailerServiceAdapter(sentry, logger)
        api['logger'] = logger
        api['logger'].setLevel(logging.DEBUG)
        api['sentry'] = sentry
        app = web.Application(middlewares=[utils.middleware.logging_catch])
        app.add_subapp('/api', api)
        api['logger'] = logger
        app['sentry'] = sentry
        setup_swagger(app, swagger_url="/doc")

    else:
        app = web.Application(
            middlewares=[utils.middleware.logging_catch]
        )
        app.router.add_routes(routes.ROUTE_TABLE)
        app['mailer_adapter'] = MailerServiceAdapter(sentry, logger)
        app['logger'] = logger
        app['sentry'] = sentry
        # Initialize redis
        app['redis'] = await aioredis.create_redis_pool(**config.Config.redis())
        app.on_shutdown.append(utils.signals.close_redis_connection)

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(TEMPLATE_FOLDER))
    return app


def run_tests():
    """
        Запуск всех тестов
    """
    raise SystemExit(pytest.main([]))


def main():
    # Parsing arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--sock', type=str, default=None)
    parser.add_argument('--tests', action='store_true', help='run all tests')
    args = vars(parser.parse_args())

    if args.get("tests"):
        return run_tests()
    loop = asyncio.get_event_loop()
    loop.set_debug(config.Config.WEB_SERVER_DEBUG)
    app = loop.run_until_complete(
        create_app(
            config.Config.WEB_SERVER_DEBUG
        )
    )

    if not args.get("sock"):
        web.run_app(
            app,
            host=config.Config.WEB_SERVER_HOST,
            port=config.Config.WEB_SERVER_PORT,
        )
    else:
        # Change mod for socket
        os.chmod(args.get("sock"), 666)

        web.run_app(
            app,
            path=args.get("sock")
        )


if __name__ == '__main__':
    main()
