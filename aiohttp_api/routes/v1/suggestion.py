# -*- coding: utf-8 -*-
from webargs import fields

from routes import make_route
from api import suggestion
from config import Config
from schemas import address


@make_route(
    input_args={
        'uuid': fields.UUID(required=True),
        'query': fields.String(required=True),
        'count': fields.Integer(missing=10, validate=lambda x: x > 0)
    },
    output_args={
        'scheme': fields.Nested(address.DaDataSuggestion, required=False)
    }
)
async def get_address(uuid, query, count, request):
    """
    ---
    description: Данный endpoint реализует функцию подсказок, а именно подсказку по адресам в соответствии с форматом ФИАС.
    tags:
    - Suggestion
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    - in: query
      name: query
      type: string
      description: Строка адреса, которую необходимо проверить и дополнить
      required: True
    - in: query
      name: count
      type: integer
      description: Количество адресов в списке подсказок
      required: False
      default: 10
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    return await suggestion.get_address_suggestions(
        query, Config.DADATA_API_KEY, count
    )
