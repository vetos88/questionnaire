# -*- coding: utf-8 -*-
from webargs import fields

from routes import make_route
from api import collection
from schemas import collection as coll


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.ForeignLanguage, many=True)
    }
)
async def get_foreign_languages(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию наименование иностранных языков.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='ForeignLanguages')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.ForeignLanguagesLevel, many=True)
    }
)
async def get_foreign_languages_levels(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию уровеней владения иностранными языками.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='ForeignLanguagesLevels')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.Education, many=True)
    }
)
async def get_education(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию тип образование и степень* (только для высшего вида образования).
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='Educations')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.EducationCertificatesType, many=True)
    }
)
async def get_education_certificates_types(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию степеней высшего образования.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='EducationCertificatesTypes')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.Interest, many=True)
    }
)
async def get_hobby(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию видов интересов из 1С системы.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='Interests')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.Country, many=True, dump_to='Citizenship', required=False, missing=[])
    }
)
async def get_citizenship(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию наименование гражданства из 1С системы.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='Countries')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.RelationDegrees, many=True)
    }
)
async def get_relation_degrees(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить коллекцию степеней родства.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection.get_1c_data(_type='RelationDegrees')


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Nested(coll.All)
    }
)
async def get_all(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить все коллекции из всех подсистем.
    tags:
    - Collection
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    """
    return await collection._all()
