# -*- coding: utf-8 -*-
from webargs import fields

from routes import make_route
from api import user


@make_route(
    input_args={
        'uuid': fields.UUID(required=True)
    },
    output_args={
        'scheme': fields.Dict(missing={}, required=True)
    }
)
async def get_data(uuid, request):
    """
    ---
    description: Данный endpoint позволяет получить JSON-схему анкеты пользователя используя UUID.
    tags:
    - User data
    produces:
    - application/json
    consumes:
    - application/x-www-form-urlencoded
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    try:
        return await user.get_data(uuid, request.app['redis'])
    except TypeError:
        return None


@make_route(
    input_args={
        'uuid': fields.UUID(required=True),
        'scheme': fields.Dict(missing={}, required=True),
    },
    output_args={
        'uuid': fields.UUID(required=True)
    }
)
async def save_data(uuid, scheme, request):
    """
    ---
    description: Данный endpoint позволяет сохранить JSON-схему анкеты пользователя используя UUID.
    tags:
    - User data
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: uuid
      type: uuid
      description: UUID пользователя
      required: True
    - in: query
      name: scheme
      type: string
      description: Сериализованный JSON анкеты
      required: True
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    return await user.save_data(uuid, scheme, request.app['redis'])
