# -*- coding: utf-8 -*-
from aiohttp import web
from webargs import fields

from routes import make_route
from api import user
from schemas import questionnaire
from utils.decorators import internal_control


@make_route(
    output_args={
        'uuid': fields.UUID(required=True)
    }
)
@internal_control
async def get_new_questionnaire(request):
    """
    ---
    description: Данный endpoint позволяет создать уникальный ИД для анкеты пользователя используя UUIDv4.
    tags:
    - Launch
    produces:
    - application/json
    consumes:
    - application/json
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    return await user.create(request.app['redis'])


@make_route(
    input_args={
        'email': fields.Email(required=True),
    },
    output_args={
        'result': fields.Nested(questionnaire.EmailCreationResponseSchema)
    }
)
async def get_new_questionnaire_link(email, request):
    """
    ---
    description: Данный endpoint позволяет создать или получить уже существующий уникальный ИД для анкеты пользователя
        используя email посредством отправки на предоставленную электронную почту.
    tags:
    - Link by Email
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    - in: query
      name: email
      type: email
      description: email для отправки
      required: True
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    email = email.lower()
    return await user.create_and_send_mail(email, request)


@make_route(
    output_args={
        'result': fields.String()
    }
)
@internal_control
async def email_status(request):
    """
    ---
    description: Endpoint предназначается для уведомления о статусе отправки почты.
    tags:
    - Link by Email
    produces:
    - application/json
    consumes:
    - application/json
    parameters:
    responses:
        "200":
            description: Successful operation.
        "404":
            description: Not found.
        "422":
            description: Unprocessable entity.
        "500":
            description: Internal error.
    """
    try:
        mailer_message = await request.json()
    except (ValueError, TypeError):
        raise web.HTTPBadRequest
    request.app.get('logger').warning('Host: {}'.format(request.host))

    for email, status in mailer_message.items():
        if status != 'OK':
            request.app['sentry'].captureMessage('Message on email {} not sent'.format(email))
    return 'received'
