"""
    Адаптер для взаимодействия с сервисом отправки почты
"""
import ujson

import aiohttp

import config


class MailerServiceAdapter:

    def __init__(self, sentry, logger):
        self.mail_worker_address = config.Config.MAIL_WORKER_ADDRESS
        self.self_service_address = config.Config.SELF_SERVICE_ADDRESS
        self.sentry = sentry
        self.logger = logger

    async def push_email_to_mailer_service(self, email, title, body):
        """
            Метод перредачи задания на отпрвки сервису отвправки почты
            {
            "distribution_data": {
                 идентификатор_задачи: {
                        "email": "емайл адресата",
                        "body": "текст письма.",
                        "subject": "заголовок письма."
                    },
                 другой_идентификатор_задачи: {
                        "email": "емайл адресата",
                        "body": "текст письма.",
                        "subject": "заголовок письма."
                    },
                 ....
                 n_идентификатор_задачи: {
                        "email": "емайл адресата",
                        "body": "текст письма.",
                        "subject": "заголовок письма."
                    },
            },
            "call_back_url": "для уведомления."
            }
            Идентификатором служит email.
        """
        data = {
            'distribution_data': {
                email: {
                    "email": email,
                    "body": body,
                    "subject": title
                }
            },
            'call_back_url': f'{self.self_service_address}/api/v1/launch.emailStatus'
        }

        # Не обрабатываем ошибки
        async with aiohttp.request(
                url=self.mail_worker_address,
                method='POST',
                headers={
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                json=data
        ) as response:
            response.raise_for_status()
            try:
                response_data = await response.json(loads=ujson.loads)
            except Exception as err:
                # уточнить типовое исклюние
                mailer_response_text = 'Bad response from mailer {}'.format(await response.text())
                self.logger.warning(mailer_response_text)
                self.sentry.captureMessage(mailer_response_text)
                response_data = {}
        return response_data
