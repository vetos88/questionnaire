# -*- coding: utf-8 -*-
from aiohttp import web
from aiohttp import log
from marshmallow import exceptions as parser_exceptions

from . import adaptor


@web.middleware
async def logging_catch(request, handler) -> web.StreamResponse:
    sentry_client = request.app['sentry']

    try:
        result = await handler(request)

        if isinstance(result, web.StreamResponse):
            return result

        else:
            return adaptor.dumps_json({
                'data': result.data,
                'error': None
            })

    except parser_exceptions.ValidationError as ex:
        sentry_client.captureException()
        log.server_logger.exception(ex)

        return adaptor.dumps_json({
            'data': None,
            'error': {
                'reason': ex.messages,
                'code': 422
            }
        })

    except web.HTTPException as ex:
        sentry_client.captureException()
        log.server_logger.exception(ex)

        return adaptor.dumps_json({
            'data': None,
            'error': {
                'reason': ex.reason,
                'code': ex.status
            }
        })

    except Exception as ex:
        sentry_client.captureException()
        log.server_logger.exception(ex)

        return adaptor.dumps_json({
            'data': None,
            'error': {
                'reason': 'Internal server error',
                'code': 500,
            }
        })
