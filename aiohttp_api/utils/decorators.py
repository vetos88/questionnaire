"""
    Обертки дополнительной функциональности
"""
from functools import wraps

from aiohttp import web

import config


def internal_control(func: callable):
    """
        Декторатор контроля внешнего и внутреннего доступа.
        Функция декоратора состоит в том чтобы чкрыть роту от вненшнего мира.
        Определение идет по параметру Host
        Ессли ожидаемая подстрока присуствует в параметре host вызывается роут
        если нет то 404
    """

    @wraps(func)
    async def coro_wrapper(*args, **kwargs):
        """
            Проверяем параметр хост.
        """
        # Специально обращение не через get ситуации когда нет request быть не должно
        request = kwargs['request']
        # Проверятся список маркеров доступного хоста
        if not any(map(lambda el: el in request.host, config.Config.SELF_SERVICE_INTERNAL_ACCESS_MARKER.split(','))):
            request.app.get('sentry').captureMessage(
                'Try access from incorrect host to internal functions. host: {}, headers: {}, url: {}'.format(
                    request.host, request.headers, request.url)
            )
            raise web.HTTPNotFound
        return await func(*args, **kwargs)

    return coro_wrapper
