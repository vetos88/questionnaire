# -*- coding: utf-8 -*-

from aiohttp import log
from aiohttp import web


async def close_redis_connection(app: web.Application) -> None:
    """
    Function performs the
    close script for redis
    instance.
    :param app: web.Application
    :return: None
    """
    log.server_logger.info('Closing redis')
    app['redis'].close()
    await app['redis'].wait_closed()
