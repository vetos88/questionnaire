# -*- coding: utf-8 -*-

import random
import string


def make_random_string(pack: str = None, n: int = 15) -> str:
    """
    Function generates random string which
    specific string cases.
    :param pack: alphabet
    :param n: length of generated string
    :return: as is
    """
    if not pack:
        pack = string.ascii_uppercase + string.digits + string.ascii_lowercase

    return ''.join(random.SystemRandom().choice(pack) for _ in range(n))
