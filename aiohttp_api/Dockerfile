FROM python:3.6.3-alpine3.6

ENV LANG=C.UTF-8

ARG WEB_SERVER_PORT=80
ARG GIT_COMMIT=unspecified
ARG GIT_BRANCH=unspecified

LABEL git_commit=${GIT_COMMIT}
LABEL git_branch=${GIT_BRANCH}
LABEL maintainers="Shirshov Ildar <shirshov.ildar@zoloto585.ru>, Kopachev Vitaly <kopachev.vitaly@zoloto585.ru>"

# Settings application environment
COPY . /src
WORKDIR /src

RUN \
# Installing systems dependecies
    apk add --update --no-cache tzdata bash && \
# Installing Python dependecies
    pip3 install pipenv -U && \
    pip3 install pip -U

# Specify timezone for correct log outputs
RUN \
    cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

RUN \
# Create virtual environment with dependencies
    apk add --update --no-cache --virtual=.build-dependencies g++ libstdc++&& \
# Installing packages from Pipfile
    set -ex && pipenv install --system && \
# Clean up the container
    apk del .build-dependencies

# Copy docker extras
COPY \
    docker-entrypoint.sh /usr/local/bin/
RUN \
    ln -s /usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh

# Port of web server
EXPOSE ${WEB_SERVER_PORT}/tcp

# Specific volume for sockets
VOLUME ["/tmp/sockets.app"]

# Flask container ready to deploy with entrypoint
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["server"]