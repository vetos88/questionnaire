# -*- coding: utf-8 -*-
from functools import partial
from raven_aiohttp import QueuedAioHttpTransport

import os
import configparser


def _try_convert(value: object, default: object, *types: tuple) -> object:
    """
    Конвертирует значение, если не выходит
    вернуть значение, возвращается
    значение поумолчанию.
    :param value: Конвертируемое значение
    :param default: Значение по-умолчанию
    :param types: Типы данных
    :return: Значение сконвертированное
    """
    if value is None:
        if callable(default):
            return default()

        return default

    for t in types:
        try:
            return t(value)
        except (TypeError, ValueError) as ex:
            continue

    return default


def _raise_config(field_name: str) -> None:
    """
    Генерирует исключение для
    поля в конфигурационном листе,
    если поле отмеченно как обязательное.
    :param field_name: Имя поля
    :return: None
    """
    raise Exception("Required option of field {}".format(field_name))


def create_config():
    """
    Создает класс конфигурации бэкэнд проекта.
    В приоритете переменные из os модуля и пользовательского
    окружения.
    :return:
    """
    CONFIG = configparser.ConfigParser(interpolation=None)
    CONFIG.read('main.ini')

    class Singleton(type):
        _instances = {}

        def __call__(cls, *args, **kwargs):
            if cls not in cls._instances:
                cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            return cls._instances[cls]

    class Config(metaclass=Singleton):

        # Опции сессии
        CSRF_SECRET_KEY = ('SECURITY', 'CSRFSecretKey', 'secret', str)
        DB_KEY_TTL = ('SECURITY', 'DBKeyTTL', 5 * 24 * 60 * 60, int)
        MASTER_KEY = ('SECURITY', 'MasterKey', 'secret', str)

        # Опции токена
        TOKEN_TTL = ('SECURITY', 'TokenTTL', 2 * 24 * 60 * 60, int)
        TOKEN_LENGTH = ('SECURITY', 'TokenLength', 24, int)

        # Опции Sentry
        SENTRY_TRANSPORT = ('SENTRY', 'Transport', 'http', str)
        SENTRY_HOST = ('SENTRY', 'Host', None, str)
        SENTRY_PORT = ('SENTRY', 'Port', 9300, int)
        SENTRY_PUBLIC_KEY = ('SENTRY', 'PublicKey', None, str)
        SENTRY_SECRET_KEY = ('SENTRY', 'SecretKey', None, str)
        SENTRY_PROJECT_VHOST = ('SENTRY', 'ProjectVHost', None, str)

        # Опции логирования
        LOGGING_LEVEL = ('LOGGING', 'Level', 'INFO', str)
        LOGGING_FORMAT = ('LOGGING', 'Format', '[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s', str)
        LOGGING_DATEFMT = ('LOGGING', 'DateFMT', '%Y-%m-%d %H:%M:%S %z', str)

        # Опции БД
        COLLECTIONS_URL = ('STORE', 'CollectionsURL', lambda: _raise_config('COLLECTIONS_URL'), str)
        REDIS_HOST = ('REDIS', 'Host', lambda: _raise_config('REDIS_HOST'), str)
        REDIS_PORT = ('REDIS', 'Port', 6379, int)

        # Опции веб сервера
        WEB_SERVER_HOST = ('WEB_SERVER', 'Host', 'localhost.localdomain', str)
        WEB_SERVER_PORT = ('WEB_SERVER', 'Port', 8080, int)
        WEB_SERVER_DEBUG = ('WEB_SERVER', 'Debug', 0, int)

        # Опции DaData API
        DADATA_API_KEY = ('DADATA', 'APIKey', lambda: _raise_config('DADATA_API_KEY'), str)
        DADATA_SECRET_KEY = ('DADATA', 'SecretKey', lambda: _raise_config('DADATA_SECRET_KEY'), str)

        # Настройки сервиса рассылки электронной почты
        MAIL_WORKER_ADDRESS = (
            'MAILER_SERVICE', 'MAIL_WORKER_ADDRESS', lambda: _raise_config('MAIL_WORKER_ADDRESS'), str
        )
        SELF_SERVICE_ADDRESS = (
            'MAILER_SERVICE', 'SELF_SERVICE_ADDRESS', lambda: _raise_config('SELF_SERVICE_ADDRESS'), str
        )

        # Внешний адрес сервиса если внешнего доступа нет указывать такой же как внутренний
        # Указывать полностью со схемой. Например https://questionnair.zoto585.ru
        SELF_SERVICE_EXTERNAL_ADDRESS = (
            'MAILER_SERVICE', 'SELF_SERVICE_EXTERNAL_ADDRESS',
            lambda: _raise_config('SELF_SERVICE_EXTERNAL_ADDRESS'), str
        )

        SELF_SERVICE_INTERNAL_ACCESS_MARKER = ('EXTERNAL_CONTROL', 'SELF_SERVICE_INTERNAL_ACCESS_MARKER', '', str)

        def __new__(cls, *args, **kwargs):
            for k, v in cls.__dict__.items():
                if isinstance(v, tuple) and len(v) == 4:
                    setattr(
                        cls, k, _try_convert(
                            os.getenv(k, CONFIG[v[0]].get(v[1])), v[2], v[3]
                        )
                    )

        @classmethod
        def logging(cls):
            return dict(
                level=cls.LOGGING_LEVEL,
                format=cls.LOGGING_FORMAT,
                datefmt=cls.LOGGING_DATEFMT
            )

        @classmethod
        def sentry(cls):
            return dict(
                dsn='{}://{}:{}@{}:{}/{}'.format(
                    cls.SENTRY_TRANSPORT,
                    cls.SENTRY_PUBLIC_KEY,
                    cls.SENTRY_SECRET_KEY,
                    cls.SENTRY_HOST,
                    cls.SENTRY_PORT,
                    cls.SENTRY_PROJECT_VHOST,
                ),
                transport=partial(QueuedAioHttpTransport, workers=4, qsize=1000)
            )

        @classmethod
        def redis(cls):
            return dict(
                address=(cls.REDIS_HOST, cls.REDIS_PORT),
                db=1,
                encoding='utf-8',
                minsize=1,
                maxsize=1
            )

        @classmethod
        def redis_for_tests(cls):
            return dict(
                address=(cls.REDIS_HOST, cls.REDIS_PORT),
                db=2,
                encoding='utf-8',
                minsize=1,
                maxsize=10
            )

    _ = Config()
    return Config


Config = create_config()
