"""
    Раные проверки эндпоинта получения ссылки на email
"""
import aioredis
import pytest
from aiohttp.test_utils import make_mocked_coro

import app
import config


@pytest.fixture()
async def api_application_client(aiohttp_client, loop):
    """
        Коиент приложения
        control_panel_app.router.get('login_endpoint').url_for()
    """
    questionnaire_app = await app.create_app()
    client = await aiohttp_client(questionnaire_app)
    return client


@pytest.fixture()
async def push_email_to_mailer_success_adapter_mock(api_application_client):
    """
        Заглушка менеджера отправки писем. Метод умпешно выполняется. Эмуляция успешной отправки.
    """
    push_to_mailer_mock = make_mocked_coro()
    api_application_client.app['mailer_adapter'].push_email_to_mailer_service = make_mocked_coro(
        'task pushed success'
    )
    return push_to_mailer_mock


@pytest.fixture()
async def push_email_to_mailer_failed_adapter_mock(api_application_client):
    """
        Заглушка менеджера отправки писем. Метод при выполнении выкидывает исключений
    """
    push_to_mailer_mock = make_mocked_coro()
    api_application_client.app['mailer_adapter'].push_email_to_mailer_service = make_mocked_coro(
        raise_exception=ConnectionError
    )
    return push_to_mailer_mock


@pytest.fixture()
async def set_redis_for_tests(api_application_client):
    """
        Изменение базы данных на базу для тестов. Запросы будут аффектить на базу.
        После тестов происходит очистка базы.
    """
    await api_application_client.app['redis'].select(config.Config.redis_for_tests()['db'])
    yield api_application_client.app['redis']
    await api_application_client.app['redis'].flushdb()


async def test_response_on_request_without_email(api_application_client, loop):
    """
        Тест ответа при запросе без email`а.
    """

    resp = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
    )
    assert resp.status == 200, 'Проверит один раз статутс т.к. у нас постоянно один статус'
    resp_body = await resp.json()
    assert resp_body.get('error') is not None, 'Ожидаем ошибку'
    assert resp_body.get('error').get('reason') is not None, 'Ожидаем контекст ошибки'
    assert resp_body.get('error').get('code') == 422, 'Код характерный для не обрабатываемых данных'


async def test_response_on_request_with_bad_email(api_application_client, loop):
    """
        Тест ответа при запросе c сплохим emailом.
    """

    resp = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': 'bad_email'}
    )
    assert resp.status == 200, 'Проверит один раз статутс т.к. у нас постоянно один статус'
    resp_body = await resp.json()
    assert resp_body.get('error') is not None, 'Ожидаем ошибку'
    assert resp_body.get('error').get('reason') is not None, 'Ожидаем контекст ошибки'
    assert resp_body.get('error').get('code') == 422, 'Код характерный для не обрабатываемых данных'


async def test_response_on_request_with_correct_email(
        api_application_client, push_email_to_mailer_success_adapter_mock, set_redis_for_tests, loop
):
    """
        Тест ответа при запросе с корректным emailом.
        Для выполнения запроса подменяется отправка к постовому сервису заглушкой.
        В редис данные заносяться реально.
    """
    provided_email = 'valid_email@emailhost.domain'

    resp = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': provided_email}
    )
    assert resp.status == 200, 'Проверит один раз статутс т.к. у нас постоянно один статус'
    resp_body = await resp.json()
    assert resp_body.get('data') is not None, 'Данные в ответе'
    assert resp_body.get('data').get('result') is not None, 'Ожидаем контекст ответа'
    assert resp_body.get('data').get('result').get('provided_email') == provided_email, 'Проверка на соответвие email`а'


async def test_response_on_request_with_correct_email_and_error_in_process(
        api_application_client,
        push_email_to_mailer_failed_adapter_mock,
        set_redis_for_tests,
        loop
):
    """
        Тест ответа при запросе с корректным emailом.
        Имитация ошибки соединения с сервисом отправки посчты
        Для выполнения запроса подменяется отправка к постовому сервису заглушкой.
        В редис данные заносяться реально.
    """
    provided_email = 'valid_email2@emailhost.domain'

    resp = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': provided_email}
    )
    assert resp.status == 200, 'Проверит один раз статутс т.к. у нас постоянно один статус'
    resp_body = await resp.json()
    assert resp_body.get('error') is not None, 'Ожидаем ошибку'
    assert resp_body.get('error').get('reason') is not None, 'Ожидаем контекст ошибки'
    assert resp_body.get('error').get('code') == 500, 'Код характерный для ошибки сервера'
