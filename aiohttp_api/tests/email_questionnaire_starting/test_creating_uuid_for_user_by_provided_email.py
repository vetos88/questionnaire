"""
    Тесты функционала создания uuid и формирования ссылки для пользователя.
"""
import uuid

import pytest
from aiohttp.test_utils import make_mocked_coro

import app
import config
from api.user import get_data, save_data


@pytest.fixture()
async def api_application_client(aiohttp_client, loop):
    """
        Клиент приложения
        control_panel_app.router.get('login_endpoint').url_for()
    """
    questionnaire_app = await app.create_app()
    client = await aiohttp_client(questionnaire_app)
    return client


@pytest.fixture()
async def push_email_to_mailer_success_adapter_mock(api_application_client):
    """
        Заглушка менеджера отправки писем. Метод умпешно выполняется. Эмуляция успешной отправки.
    """
    push_to_mailer_mock = make_mocked_coro('task pushed success')
    api_application_client.app['mailer_adapter'].push_email_to_mailer_service = push_to_mailer_mock
    return push_to_mailer_mock


@pytest.fixture()
async def redis_for_tests(api_application_client):
    """
        Изменение базы данных на базу для тестов. Запросы будут аффектить на базу.
        После тестов происходит очистка базы.
    """
    await api_application_client.app['redis'].select(config.Config.redis_for_tests()['db'])
    yield api_application_client.app['redis']
    await api_application_client.app['redis'].flushdb()


"""
    Тесты взаимодействия с БД
"""


async def test_creation_new_email_uuid_pair(
        api_application_client,
        push_email_to_mailer_success_adapter_mock,
        redis_for_tests,
        loop
):
    """
        Проверка логики создания записей для нового email`a.
        Проверка отправки новых данных.
        Проверка агрументов вызова отпарвки email`a
    """
    provided_email = 'test_valid_email@emailhost.domain'

    assert not await redis_for_tests.exists('email_{}'.format(provided_email)), \
        'Результат запроса должен быть ложным. Ключ соответвующий предоставленному email`у должен отсутствовать'

    _ = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': provided_email}
    )
    assert await redis_for_tests.exists('email_{}'.format(provided_email)), \
        'Результат запроса должен быть истииным. Ключ соответвующий предоставленному email`у должен поятвиться в бд'

    # Достаем созданный uuid.
    _uuid = await redis_for_tests.get('email_{}'.format(provided_email))

    # Достаем пользовательские данные.
    user_data = await get_data(_uuid, redis_for_tests)
    # Данная проверка не столько проверяет тип,0
    # cколько видедельствует о том что если до этого не было ошибок значит все ок
    assert isinstance(user_data, dict), 'Должен быть словарь'
    # Проверка вызовова отправки почты
    assert push_email_to_mailer_success_adapter_mock.call_count == 1
    assert push_email_to_mailer_success_adapter_mock.call_args[0][0] == provided_email
    assert push_email_to_mailer_success_adapter_mock.call_args[0][1] == "Анкета кандидата"
    assert _uuid in push_email_to_mailer_success_adapter_mock.call_args[0][2]


async def test_no_creation_new_entries_if_pair_already_exist(
        api_application_client, push_email_to_mailer_success_adapter_mock, redis_for_tests, loop
):
    """
        Проверка логики обработки уже существующих записей.
        Записи не должны перезаписываться
    """
    # Создаем записи
    provided_email = 'test_valid_email@emailhost.domain'

    used_data_with_marker = {
        "marker": "control_marker"
    }

    # Создаем тестовые фикстуры
    _uuid = uuid.uuid4()
    user_email = 'email_{}'.format(provided_email)
    _ = await redis_for_tests.set(user_email, str(_uuid), expire=config.Config.DB_KEY_TTL)
    _ = await save_data(_uuid, used_data_with_marker, redis_for_tests)

    # Делаем запрос на создание данных
    _ = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': provided_email}
    )
    # Проверяем значение uuid
    existing_uuid = await redis_for_tests.get('email_{}'.format(provided_email))
    assert str(_uuid) == existing_uuid, 'Uuid должен соответствовать email`y'

    # Проверяем пользоваетльские данные
    user_data = await get_data(_uuid, redis_for_tests)

    assert user_data.get("marker") == used_data_with_marker.get("marker"), \
        "Пользовательские данные не должны поменяться"

    # Проверка вызовова отправки почты
    assert push_email_to_mailer_success_adapter_mock.call_count == 1
    assert push_email_to_mailer_success_adapter_mock.call_args[0][0] == provided_email
    assert push_email_to_mailer_success_adapter_mock.call_args[0][1] == "Анкета кандидата"
    assert str(_uuid) in push_email_to_mailer_success_adapter_mock.call_args[0][2]


async def test_recreation_new_entries_if_user_data_not_exist(
        api_application_client, push_email_to_mailer_success_adapter_mock, redis_for_tests, loop
):
    """
        Тест исключительной ситуации если uuid по email`у есть, а пользоваетльских данных нет.
    """
    # Создаем записи
    provided_email = 'test_valid_email@emailhost.domain'

    # Создаем тестовые фикстуры. Создаем только запись с email`ом
    _uuid_for_not_existing_user_data = uuid.uuid4()
    user_email = 'email_{}'.format(provided_email)
    _ = await redis_for_tests.set(user_email, str(_uuid_for_not_existing_user_data), expire=config.Config.DB_KEY_TTL)

    # проверяем что пользовательских данных нет
    user_data_not_exist = False
    try:
        _ = await get_data(_uuid_for_not_existing_user_data, redis_for_tests)
    except TypeError:
        user_data_not_exist = True

    assert user_data_not_exist, 'Пользовательские данные не должны существовать'

    # Делаем запрос на создание данных
    _ = await api_application_client.post(
        '/v1/launch.getNewQuestionnaireLink',
        json={'email': provided_email}
    )

    # Проверяем значение uuid
    new_generated_uuid = await redis_for_tests.get('email_{}'.format(provided_email))
    assert str(_uuid_for_not_existing_user_data) != new_generated_uuid, 'Uuid должен быть новый'

    # Проверяем пользоваетльские данные, Если не вылетит ошибка значит все хорошо
    _ = await get_data(new_generated_uuid, redis_for_tests)

    # Проверка вызовова отправки почты
    assert push_email_to_mailer_success_adapter_mock.call_count == 1
    assert push_email_to_mailer_success_adapter_mock.call_args[0][0] == provided_email
    assert push_email_to_mailer_success_adapter_mock.call_args[0][1] == "Анкета кандидата"
    assert new_generated_uuid in push_email_to_mailer_success_adapter_mock.call_args[0][2]
