# -*- coding: utf-8 -*-
import datetime
import uuid
import ujson

import aiohttp_jinja2
from aiohttp import web
from aioredis import Redis

import config
from schemas import questionnaire


async def create(redis_instance) -> str:
    """
    Функция генерирует uuid по-запросу
    от фронта на случай открытия новой
    анкеты.
    :param redis_instance: Экземпляр Redis службы
    :return: Уникальный ИД пользователя
    """
    assert redis_instance and isinstance(redis_instance, Redis)

    while True:
        _uuid = uuid.uuid4()

        try:
            _ = await get_data(_uuid, redis_instance)
        except TypeError:
            return await save_data(_uuid, {}, redis_instance)
        else:
            continue


async def create_and_send_mail(email, request) -> dict:
    """
    Функция принимает запрос от пользователя на гененрацию новой анкеты.
    Получает от пользователя email.
    Если этого адреса почты нет в базе, то генерируется ссылка на основе нового uuid.
    Если есть то генерируется ссылка на основе существующего uuid
    Генерируется шаблон и передается в очередь на отправку
    :param request: обьект запроса
    :param email: Email предоставленные пользователем
    :return: Уникальный ИД пользователя
    """
    redis_instance = request.app.get('redis')
    assert redis_instance and isinstance(redis_instance, Redis)

    # Пробуем найти есть ли такой email в базе
    _uuid = await redis_instance.get('email_{}'.format(email))

    if not _uuid:
        # Если нет создаем для него uuid
        _uuid = await create_email_uuid_pair(email, redis_instance)
    else:
        # Если есть проверяем есть ли под нее запись с анкетой
        try:
            _ = await get_data(_uuid, redis_instance)
        except TypeError:
            # Если записи нет то считаем что это исключительная ситуация
            # и производим процедуру регистрации получения нового uuid
            _uuid = await create_email_uuid_pair(email, redis_instance)

    mail_context = {
        'host': config.Config.SELF_SERVICE_EXTERNAL_ADDRESS,
        '_uuid': _uuid
    }

    rendered_template = aiohttp_jinja2.render_string('questionnaire_candidate_mail_text.html', request, mail_context)

    _ = await request.app.get('mailer_adapter').push_email_to_mailer_service(
        email,
        "Анкета кандидата",
        rendered_template
    )

    return questionnaire.EmailCreationResponseSchema().load({
        'status': 'ok',
        'provided_email': email,
        'created_at_utc': datetime.datetime.utcnow().isoformat()
    })


async def create_email_uuid_pair(email: str, redis_instance) -> str:
    """
        Создание пары uuid email и записи по анкету
    """
    _uuid = await create(redis_instance)
    # И сохраняем пару email - uuid
    user_email = 'email_{}'.format(email)
    await redis_instance.set(user_email, str(_uuid), expire=config.Config.DB_KEY_TTL)
    return _uuid


async def get_data(_uuid, redis_instance) -> object:
    """
    Функция выгружает данные анкеты
    для указанного пользователя uuid.
    :param _uuid: ИД пользователя
    :param redis_instance: Экземпляр Redis службы
    :param partial: Игнорировать отсутствующие элементы
    :return: Анкета пользователя в строковом представлении
    """
    assert redis_instance and isinstance(redis_instance, Redis)

    val = await redis_instance.get('user_{}'.format(_uuid))
    return ujson.loads(val)


async def save_data(_uuid, data: dict, redis_instance) -> str:
    """
    Функция сохраняет данные анкеты
    для указанного пользователя в RDB
    базе.
    :param uuid: Уникальный ИД пользователя
    :param data: Данные анкеты
    :param redis_instance: Экземпляр Redis службы
    :param partial: Игнорировать отсутствующие элементы
    :return: Уникальный ИД пользователя
    """
    assert redis_instance and isinstance(redis_instance, Redis)
    user_id = 'user_{}'.format(_uuid)

    # Сохраняем структуру в постоянное хранилище
    await redis_instance.set(user_id, ujson.dumps(data), expire=config.Config.DB_KEY_TTL)

    # Помещаем сообщение до востребования в очередь
    # Если в обьекте нет processingPersonalDataAgreement то считаем что это инициализация новой анкеты
    # Сохраняем только в том случае если значение true.
    # Если не True кидаем ошибку
    if data.get('processingPersonalDataAgreement') is True:
        await redis_instance.sadd('users.s_queue', user_id)
    else:
        if data.get('processingPersonalDataAgreement') is not None:
            raise web.HTTPBadRequest

    return _uuid
