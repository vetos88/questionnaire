# -*- coding: utf-8 -*-
import ujson
from aiohttp import request
from aiohttp import web_exceptions

from schemas import collection

import config


async def get_1c_data(_type: str = 'All') -> dict:
    _types = (
        'All',
        'Countries',
        'Interests',
        'Educations',
        'EducationCertificatesTypes',
        'ForeignLanguages',
        'ForeignLanguagesLevels',
        'RelationDegrees',
    )

    assert _type in _types

    async with request(
        url=config.Config.COLLECTIONS_URL,
        method='GET',
        headers={
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    ) as response:
        response.raise_for_status()

        response_data = await response.json(loads=ujson.loads)

        if response_data['error'] not in ('null', ''):
            raise web_exceptions.HTTPForbidden(reason=response_data['error']['reason'])
        else:
            parsed = collection.All().load(response_data['data']['scheme'])
            if _type != 'All':
                return parsed.data[_type]
            return parsed


async def _all() -> dict:
    return await get_1c_data()
