# -*- coding: utf-8 -*-
import ujson
import aiohttp

from schemas import address


async def get_address_suggestions(query_data: str, api_token: str, count: int = 10) -> dict:
    async with aiohttp.request(
            url='https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
            method='POST',
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Token {}'.format(api_token)
            },
            data=ujson.dumps({'query': query_data, 'count': count})
    ) as response:
        response.raise_for_status()

        response_data = await response.json(loads=ujson.loads)
        return address.dadata.load(response_data)
