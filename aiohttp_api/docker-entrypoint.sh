#!/usr/bin/env bash
set -e

: ${SOCKET_DIRECTORY:="/tmp/sockets.app"}

if [[ "${1}" == "socket" ]]; then

    if [[ ! -d "${SOCKET_DIRECTORY}" ]]; then
        mkdir -p "${SOCKET_DIRECTORY}"
    fi

    python3 app.py --sock "${SOCKET_DIRECTORY}/aiohttp_api.sock"
fi

if [[ "${1}" == "server" ]]; then
    python3 app.py
fi