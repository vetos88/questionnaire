# -*- coding: utf-8 -*-
from marshmallow import Schema
from marshmallow import fields
from marshmallow import ValidationError
from marshmallow import validates_schema


class Base(Schema):

    class Meta:
        dateformat = '%Y-%m-%d'

    @validates_schema(pass_original=True)
    def check_unknown_fields(self, data, original_data):
        for x in ([original_data] if isinstance(original_data, dict) else original_data):
            unknown = set(x) - set(self.fields)
            if unknown:
                raise ValidationError('Unknown field', list(unknown))


class Education(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class EducationCertificatesType(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class RelationDegrees(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class ForeignLanguage(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class ForeignLanguagesLevel(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class Interest(Base):

    id = fields.UUID(required=True, dump_to='uuid')
    name = fields.String(required=True, dump_to='text')


class Country(Base):

    id = fields.Int(required=True, dump_to='id')
    name = fields.String(required=True, dump_to='text')


class All(Base):

    ForeignLanguages = fields.Nested(ForeignLanguage, many=True, required=True)
    ForeignLanguagesLevels = fields.Nested(ForeignLanguagesLevel, many=True, required=True)
    Interests = fields.Nested(Interest, many=True, required=True)
    Educations = fields.Nested(Education, many=True, required=True)
    EducationCertificatesTypes = fields.Nested(EducationCertificatesType, many=True, required=True)
    RelationDegrees = fields.Nested(RelationDegrees, many=True, required=True)
    Countries = fields.Nested(Country, many=True, required=False, missing=[], dump_to='Citizenship')


collections_all = All()