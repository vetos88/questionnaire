# -*- coding: utf-8 -*-
import re

from marshmallow import Schema
from marshmallow import ValidationError
from marshmallow import fields
from marshmallow import validates
from marshmallow import validates_schema

CYRILLIC_LETTERS_RE = re.compile(r'^[а-яА-ЯЁё-]+$')


def only_cyrillic(x):
    return True if CYRILLIC_LETTERS_RE.match(x) or x == '' else False


def validate_email(x):
    return True if x == '' or len(str(x).split('@')) == 2 else False


def is_capitalized(x):
    return x.istitle() or x == ''


def valid_document_types(x):
    return x in ('RF_PASSPORT', 'RESIDENCE_PERMIT', 'ANOTHER_COUNTRY_PASSPORT')


class Base(Schema):
    class Meta:
        dateformat = '%Y-%m-%d'

    @validates_schema(pass_original=True)
    def check_unknown_fields(self, data, original_data):
        for x in ([original_data] if isinstance(original_data, dict) else original_data):
            unknown = set(x) - set(self.fields)
            if unknown:
                raise ValidationError('Unknown field', list(unknown))


class FullName(Base):
    name = fields.String(required=True, validate=[only_cyrillic, is_capitalized])
    surname = fields.String(required=True, validate=[only_cyrillic, is_capitalized])
    middleName = fields.String(validate=[only_cyrillic, is_capitalized], allow_none=True)


class ForeignLanguages(Base):
    title = fields.String(required=True)
    level = fields.String(required=True)


class WorkingOrganization(Base):
    dateFrom = fields.Date()
    untilNow = fields.Boolean(default=False)
    dateTo = fields.Date()
    title = fields.String()
    activityDirection = fields.String()
    position = fields.String()
    officialDuties = fields.String()
    subordinatesAmount = fields.Integer(allow_none=True)
    salaryLevel = fields.Integer(allow_none=True)
    firingReasons = fields.String()


class EducationInstitution(Base):
    dateFrom = fields.String(required=True)
    untilNow = fields.Boolean(required=True, default=False)
    dateTo = fields.String(allow_none=True)
    institutionName = fields.String(required=True)
    specialty = fields.String(required=True)
    qualification = fields.String(required=True)
    educationLevelType = fields.String(required=True)


class PreviousLiveLocation(Base):
    locatedPeriod = fields.String()
    liveLocation = fields.String()
    dateFrom = fields.String(allow_none=True)
    dateTo = fields.String(allow_none=True)


class RelativeData(FullName):
    birthDay = fields.Date()
    relationDegree = fields.String()
    bornLocation = fields.String()
    liveLocation = fields.String()
    workLocation = fields.String()
    workPosition = fields.String()
    telephone = fields.String()
    isDisabled = fields.Boolean()
    disabledGroup = fields.String()

    @validates('relationDegree')
    def validate_relation_degree(self, value):
        return True  # FIXME: Add validator

    @validates('bornLocation')
    def validate_born_location(self, value):
        return True  # FIXME: Add validator

    @validates('workLocation')
    def validate_born_location(self, value):
        return True  # FIXME: Add validator

    @validates('disabledGroup')
    def validate_born_location(self, value):
        return True  # FIXME: Add validator


class PreviousPersonalData(FullName):
    changeDate = fields.String(allow_none=True)


class Questionnaire(FullName):
    # Фамилия Имя Отчество
    wasPreviousPersonalData = fields.Boolean(required=True, default=False)
    previousPersonalData = fields.Nested(PreviousPersonalData, many=True, missing=[])

    # Характеристики кандидата
    genderSign = fields.String(required=True)
    bornLocation = fields.String(required=True)
    birthDay = fields.Date(required=True)
    registeredLiveLocation = fields.String(required=True)
    sameAsRegisteredLive = fields.Boolean(default=False)
    liveLocation = fields.String(required=True)
    maritalStatus = fields.String(required=True)
    isDisabled = fields.Boolean(default=False, required=True)
    disabledGroup = fields.String(allow_none=True)
    mobileTelephone = fields.String(default='MARITAL_STATUS_NOT_MARRIED', required=True)
    homeTelephone = fields.String(allow_none=True)
    workTelephone = fields.String(allow_none=True)
    email = fields.String(validate=[validate_email], allow_none=True)
    snils = fields.String(allow_none=True)
    inn = fields.String(allow_none=True)

    # Дополнительные вопросы
    pcKnowledgeLevel = fields.String(allow_none=True)
    whereFindVacancy = fields.String(required=True)
    additionalNotation = fields.String(allow_none=True)
    personalHobby = fields.List(fields.String(), required=True, missing=[], many=True)
    salaryLevelWantedNow = fields.Integer()
    salaryLevelWantedBeforeYear = fields.String()
    salaryLevelWantedBeforeThreeYears = fields.String()
    checkPersonalDataAgreementByPolygraph = fields.Boolean(default=False, required=True)
    badHabitsHaving = fields.Boolean(default=False, required=True)
    criminalConvictionAdministrativeOffenses = fields.Boolean(default=False, required=True)

    # История проживания
    previousLiveLocationsWas = fields.Boolean(required=True, default=False)
    previousLiveLocations = fields.Nested(PreviousLiveLocation, missing=[], many=True)

    # Документы
    currentPersonalDocumentType = fields.String(default='RF_PASSPORT', validate=valid_document_types)
    passportSeries = fields.String(allow_none=True)
    passportNumber = fields.String(allow_none=True)
    passportIssueDate = fields.Date(allow_none=True)
    passportIssueOrganization = fields.String(allow_none=True)

    residencePermitSeries = fields.String(allow_none=True)
    residencePermitNumber = fields.String(allow_none=True)
    residencePermitIssueDate = fields.Date(allow_none=True)
    residencePermitIssueOrganization = fields.String(allow_none=True)

    anotherCountryCitizenship = fields.String(allow_none=True)
    anotherCountryPassportSeries = fields.String(allow_none=True)
    anotherCountryPassportNumber = fields.String(allow_none=True)
    anotherCountryPassportIssueDate = fields.Date(allow_none=True)
    anotherCountryPassportIssueOrganization = fields.String(allow_none=True)

    # Родственники
    relativesData = fields.Nested(RelativeData, missing=[], many=True)

    # Образование
    educationalInstitutions = fields.Nested(EducationInstitution, missing=[], many=True)

    # Опыт работы
    workingOrganizations = fields.Nested(WorkingOrganization, missing=[], many=True)

    # Владение языками
    foreignLanguages = fields.Nested(ForeignLanguages, missing=[], many=True)

    processingPersonalDataAgreement = fields.Boolean(default=False, required=True)


class EmailCreationResponseSchema(Schema):
    status = fields.Str(required=True)
    provided_email = fields.Email(required=True)
    created_at_utc = fields.String(required=True)
