# -*- coding: utf-8 -*-
from marshmallow import Schema
from marshmallow import fields


class DaDataAddressData(Schema):

    class Meta:
        dateformat = '%Y-%m-%d'

    postal_code = fields.String()               # Индекс
    county = fields.String()                    # Страна
    region_fias_id = fields.String()            # Код ФИАС региона
    region_kladr_id = fields.String()           # Код КЛАДР региона
    region_with_type = fields.String()          # Регион с типом
    region_type = fields.String()               # Тип региона(сокращенный)
    region_type_full = fields.String()          # Тип региона
    region = fields.String()                    # Регион
    area_fias_id = fields.String()              # Код ФИАС района в регионе
    area_kladr_id = fields.String()             # Код КЛАДР района в регионе
    area_with_type = fields.String()            # Район в регионе с типом
    area_type = fields.String()                 # Тип района в регионе(сокращенный)
    area_type_full = fields.String()            # Тип района в регионе
    area = fields.String()                      # Район в регионе
    city_fias_id = fields.String()              # Код ФИАС города
    city_kladr_id = fields.String()             # Код КЛАДР города
    city_with_type = fields.String()            # Город с типом
    city_type = fields.String()                 # Тип города(сокращенный)
    city_type_full = fields.String()            # Тип города
    city = fields.String()                      # Город
    city_district_fias_id = fields.String()     # Код ФИАС района города(заполняется, только если район есть в ФИАС)
    city_district_kladr_id = fields.String()    # Код КЛАДР района города(не заполняется)
    city_district_with_type = fields.String()   # Район города с типом
    city_district_type = fields.String()        # Тип района города(сокращенный)
    city_district_type_full = fields.String()   # Тип района города
    city_district = fields.String()             # Район города
    settlement_fias_id = fields.String()        # Код ФИАС нас.пункта
    settlement_kladr_id = fields.String()       # Код КЛАДР нас.пункта
    settlement_with_type = fields.String()      # Населенный пункт с типом
    settlement_type = fields.String()           # Тип населенного пункта(сокращенный)
    settlement_type_full = fields.String()      # Тип населенного пункта
    settlement = fields.String()                # Населенный пункт
    street_fias_id = fields.String()            # Код ФИАС улицы
    street_kladr_id = fields.String()           # Код КЛАДР улицы
    street_with_type = fields.String()          # Улица с типом
    street_type = fields.String()               # Тип улицы(сокращенный)
    street_type_full = fields.String()          # Тип улицы
    street = fields.String()                    # Улица
    house_fias_id = fields.String()             # Код ФИАС дома
    house_kladr_id = fields.String()            # Код КЛАДР дома
    house_type = fields.String()                # Тип дома(сокращенный)
    house_type_full = fields.String()           # Тип дома
    house = fields.String()                     # Дом
    block_type = fields.String()                # Тип корпуса / строения(сокращенный)
    block_type_full = fields.String()           # Тип корпуса / строения
    block = fields.String()                     # Корпус / строение
    flat_type = fields.String()                 # Тип квартиры(сокращенный)
    flat_type_full = fields.String()            # Тип квартиры
    flat = fields.String()                      # Квартира
    postal_box = fields.String()                # Абонентский ящик
    fias_id = fields.String()                   # Код ФИАС:
                                                                # HOUSE.HOUSEGUID, если дом найден в ФИАС по точному совп адению;
                                                                # ADDROBJ.AOGUID в противном случае.

    fias_level = fields.String()                # Уровень детализации, до которого адрес найден в ФИАС:
                                                                # 0 — страна
                                                                # 1 — регион
                                                                # 3 — район
                                                                # 4 — город
                                                                # 5 — район города
                                                                # 6 — населенный пункт
                                                                # 7 — улица
                                                                # 8 — дом
                                                                # 65 — планировочная структура
                                                                # -1 — иностранный или пустой

    kladr_id = fields.String()                  # Код КЛАДР
    geoname_id = fields.String()                # Идентификатор объекта в базе GeoNames.
                                                                              # Для российских адресов не заполняется.
    capital_marker = fields.String()            # Признак центра района или региона:
                                                                # 1 — центр района(Московская обл, Одинцовский р - н, г Одинцово)
                                                                # 2 — центр региона(Новосибирская обл, г Новосибирск)
                                                                # 3 — центр района и региона(Томская обл, г Томск)
                                                                # 4 — центральный район региона(Тюменская обл, Тюменский р - н)
                                                                # 0 — ничего из перечисленного(Московская обл, г Балашиха)

    okato = fields.String()                     # Код ОКАТО
    oktmo = fields.String()                     # Код ОКТМО
    tax_office = fields.String()                # Код ИФНС для физических лиц
    tax_office_legal = fields.String()          # Код ИФНС для организаций


class DaDataAddress(Schema):

    class Meta:
        exclude = ('data', )
        dateformat = '%Y-%m-%d'

    address = fields.String(required=True, load_from='value')                    # Адрес одной строкой (как показывается в списке подсказок)
    full_address = fields.String(required=True, load_from='unrestricted_value')  # Адрес одной строкой (полный, от региона)
    data = fields.Nested(DaDataAddressData, required=True)


class DaDataSuggestion(Schema):

    class Meta:
        ordered = True
        dateformat = '%Y-%m-%d'

    suggestions = fields.Nested(DaDataAddress, many=True)


dadata = DaDataSuggestion()
