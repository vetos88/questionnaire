# Анкета кандидата - EQC(electronic questionnaire candidate)

Логически проект состоит из 3 частей: 
    
    - SPA. Клиентское JS приложение. 
    - API для сохранения и получения данных. 
    - Обработчик очереди сохраненных данных об анкетах.

## Запуск в окружении разработчика

Получение кода:
```bash
git clone git@bitbucket.org:zoloto585ru/questionnaire.git
git checkout dev
cd questionnaire
```

Бекенд:  

Для работы АПИ требуется REDIS.
Прелагается сценарий для запуска при помощи технологии Docker.

```bash
docker run --name questionnaire_redis \
    -p 6381:6379 -d \
    redis redis-server --appendonly yes
```

```bash
# Создаем окружение
pipenv --python 3.6
```

Настройка конфигураций осуществляется путем создания файла ```main.ini``` в корне папки соответвующего сервиса.   
Доступные настройки и их описание находиться в файле ```main.ini.example```.    
ФАКТИЧЕСКИЕ настройки можно посмотреть в хранилище Consul ```Key/Values < zoloto585.int < dev < service < hr.questionnaire```.

1 API:

```bash
cd aiohttp_api
# Устанавливаем зависимости
pipenv install --dev
# Запуск приложения
python3 app.py
```

2 Обработчик:

```bash
cd loop_push
# Устанавливаем зависимости
pipenv install --dev
# Запуск приложения
python3 app.py
```

3 JS SPA:

```bash
cd web-app/staff-questionnaire-web
# Устанавливаем зависимости
npm install
# Запуск приложения
npm run dev
```

## Сборка проекта.

Поскольку эксплуатация сервисов предполагается в Docker-контейнерах. На базе исходного кода требуется собрать Docker-образ.

Для работы с докер регистри можно задать конфигурацию регистри в файле ```build_env```, пример в ```build_env.example```.

### Сборка.

Возможна сборка как всех образов сразу
```bash
./build_manager.sh build-all
``` 
Так и с указанием конкретного образа
Например:
```bash
./build_manager.sh aiohttp_api
```

## Развертывание

Скрипты и конфигурации развертывания находяться в папке `deployment`.

```bash
cd deployment
```

## Сценарии процесса развёртывания предполагают использование технологии Docker-swarm.

## Настройка

Конфигурация процесса развёртывания предполагает наличие файла ```stack_config```, пример ```stack_config.example```.  
Либо целевые параметры должны быть доступны через переменные окружения.  
ФАКТИЧЕСКИЕ настройки можно посмотреть в хранилище Consul   

    - dev: ```Key/Values < zoloto585.int < dev < service <  hr.questionnaire < stack_config```.
    - prod: ```Key/Values < zoloto585.int < prod < service <  hr.questionnaire < stack_config```.

Конфигурация параметров требуемых при работе сервиса.  
Настройка конфигурации осуществляется путем создания файлов ```aiohttp_api_main.ini``` и ```loop_poosh_main.ini``` в папке со скриптом развертывания.   
Доступные настройки и их описание находятся в папках целевых сервисов в файле ```main.ini.example```.   
Эти параметры дублируются из целевого репозитория сервиса.  
ФАКТИЧЕСКИЕ настройки можно посмотреть в хранилище Consul 

    - dev: ```Key/Values < zoloto585.int < dev < service < hr.questionnaire```.
    - prod: ```Key/Values < zoloto585.int < prod < service < hr.questionnaire```.

## Запуск

```bash
./deploy_manager.sh run
```
## Проверка статуса

```bash
./deploy_manager.sh status
```